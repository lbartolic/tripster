package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by asus on 13.08.16..
 */
public interface UserInterface {
    @GET("{api_key}")
    Call<User> getUser(@Path("api_key") String apiKey);

    @FormUrlEncoded
    @POST("auth")
    Call<User> postAuth(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("register")
    Call<User> postRegister(@Field("email") String email, @Field("password") String password,
                            @Field("password_repeat") String passwordRepeat, @Field("first_name") String firstName,
                            @Field("last_name") String lastName);
}
