package com.example.asus.tripplanner.models;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.example.asus.tripplanner.R;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 20.08.16..
 */
public class TransportationType {
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("type_name")
    private String typeName;

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public Drawable getIcon(Context context) {
        Drawable drawable = null;
        switch (this.getType()) {
            case "car":
                drawable = context.getResources().getDrawable(R.drawable.ic_directions_car_black_24dp);
                break;
            case "flight":
                drawable = context.getResources().getDrawable(R.drawable.ic_flight_black_24dp);
                break;
            case "other":
                drawable = null;
                break;
        }
        return drawable;
    }
}
