package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Event;

import java.util.List;

/**
 * Created by asus on 17.08.16..
 */
public interface EventListResponse {
    void onSuccess(List<Event> eventList);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
