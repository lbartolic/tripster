package com.example.asus.tripplanner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.fragments.PastTripsFragment;
import com.example.asus.tripplanner.fragments.TripDayFragment;
import com.example.asus.tripplanner.fragments.UpcomingTripsFragment;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.utils.JsonParseUtil;

import java.util.List;

/**
 * Created by asus on 15.08.16..
 */
public class DayPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private Trip trip;
    private List<TripDay> tripDays;

    public DayPagerAdapter(FragmentManager fm, Context ctx, Trip trip) {
        super(fm);
        this.context = ctx;
        this.trip = trip;
        this.tripDays = trip.getTripDays();
    }

    @Override
    public Fragment getItem(int position) {
        TripDayFragment tdf = new TripDayFragment();
        Bundle bundle = new Bundle();
        bundle.putString("TRIP_DAY", JsonParseUtil.convertObjectToJson(tripDays.get(position)));
        bundle.putInt("TRIP_DAY_POS", position);
        bundle.putString("TRIP", JsonParseUtil.convertObjectToJson(trip));
        tdf.setArguments(bundle);
        return tdf;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "DAY " + (position + 1);
    }

    @Override
    public int getCount() {
        return tripDays.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
