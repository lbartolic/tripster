package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by asus on 17.08.16..
 */
public interface EventInterface {
    @FormUrlEncoded
    @POST("{api_key}/trips/{tripId}/tripDays/{tripDayId}/events")
    Call<Event> createOrEdit(@Path("api_key") String api_key, @Path("tripId") int tripId,
                       @Path("tripDayId") int tripDayId, @Field("eventId") int eventId,
                             @Field("event_type") String eventType, @Field("title") String title,
                       @Field("from_time") String startTime, @Field("to_date") String dateTo,
                       @Field("to_time") String timeTo, @Field("expense_amount") String expense,
                       @Field("description") String description, @Field("confirmation_code_acc") String confirmationCodeAcc,
                       @Field("host") String host,
                       @Field("confirmation_code_tra") String confirmationCodeTra,
                       @Field("transport_number") String transportNumber, @Field("airline") String airline,
                       @Field("terminal") String terminal, @Field("gate") String gate,
                       @Field("transportation_type") String transportationType,
                       @Field("place_identifier") String mapPlaceId, @Field("place_name") String mapPlaceName,
                       @Field("place_address") String mapPlaceAddress, @Field("place_lat") String mapPlaceLat,
                       @Field("place_lng") String mapPlaceLng);

    @GET("{api_key}/trips/{tripId}/tripDays/{tripDayId}/events/{eventId}")
    Call<Event> event(@Path("api_key") String apiKey, @Path("tripId") int tripId, @Path("tripDayId") int tripDayId, @Path("eventId") int eventId);

    @DELETE("{api_key}/trips/{tripId}/tripDays/{tripDayId}/events/{id}")
    Call<Event> destroyEvent(@Path("api_key") String api_key, @Path("tripId") int tripId,
                                 @Path("tripDayId") int tripDayId, @Path("id") int eventId);
}
