package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;

import java.util.List;

/**
 * Created by asus on 17.08.16..
 */
public interface TripListResponse {
    void onSuccess(List<Trip> tripList);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
