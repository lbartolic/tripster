package com.example.asus.tripplanner.models;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;

import com.example.asus.tripplanner.R;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by asus on 16.08.16..
 */
public class Event {
    private static final String PREFS_KEY = "events";

    @SerializedName("id")
    private int id;
    @SerializedName("eventable_id")
    private int childId;
    @SerializedName("eventable_type")
    private String childType;
    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private User user;
    @SerializedName("description")
    private String description;
    @SerializedName("from_time")
    private String fromTime;
    @SerializedName("to_date")
    private String toDate;
    @SerializedName("to_time")
    private String toTime;
    @SerializedName("expense_amount")
    private String expenseAmount;
    @SerializedName("trip_day")
    private TripDay tripDay;
    @SerializedName("trip_day_id")
    private int tripDayId;
    @SerializedName("eventable")
    private EventChild eventChild;
    @SerializedName("event_type")
    private EventType eventType;
    @SerializedName("google_place")
    private GooglePlace googlePlace;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public int getChildId() {
        return childId;
    }

    public String getChildType() {
        return childType;
    }

    public String getTitle() {
        return title;
    }

    public User getUser() {
        return user;
    }

    public String getDescription() {
        return description;
    }

    public String getFromTime() {
        return fromTime;
    }

    public String getToDate() {
        return toDate;
    }

    public String getToTime() {
        return toTime;
    }

    public String getExpenseAmount() {
        return expenseAmount;
    }

    public TripDay getTripDay() {
        return tripDay;
    }

    public int getTripDayId() {
        return tripDayId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public EventChild getEventChild() {
        return eventChild;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Drawable getChildTypeIcon(Context context) {
        EventType type = this.eventType;
        Drawable drawable = type.getIcon(context);
        return drawable;
    }

    public GooglePlace getGooglePlace() {
        return googlePlace;
    }
}
