package com.example.asus.tripplanner.models;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.example.asus.tripplanner.R;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 20.08.16..
 */
public class GooglePlace {
    public static final float DEFAULT_ZOOM = 11;

    @SerializedName("id")
    private int id;
    @SerializedName("place_identifier")
    private String placeIdentifier;
    @SerializedName("place_name")
    private String placeName;
    @SerializedName("place_address")
    private String placeAddress;
    @SerializedName("place_lat")
    private Double placeLat;
    @SerializedName("place_lng")
    private Double placeLng;
    @SerializedName("event")
    private Event event;

    public int getId() {
        return id;
    }

    public String getPlaceIdentifier() {
        return placeIdentifier;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getPlaceAddress() {
        return placeAddress;
    }

    public Double getPlaceLat() {
        return placeLat;
    }

    public Double getPlaceLng() {
        return placeLng;
    }

    public Event getEvent() {
        return event;
    }
}
