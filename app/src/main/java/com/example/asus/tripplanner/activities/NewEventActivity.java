package com.example.asus.tripplanner.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.EventType;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.ValidationUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.EventRepository;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewEventActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener, Validator.ValidationListener {
    private Trip trip;
    private EventType eventType;
    private Event editEvent;
    private int editTripDayPos;
    private ProgressDialog progressDialog;
    private CoordinatorLayout clNewEvent;
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private Button btnAddMapLoc;
    private Button btnFormMore;
    private Button btnSave;
    private TextView tvHeading;
    private TextView tvTitle;
    private TextView tvStartsAt;
    private TextView tvEndsAt;
    private TextView tvExpense;
    private TextView tvCurrency;
    private TextView tvDescription;
    private TextView tvMapLocName;
    @NotEmpty
    private EditText etTitle;
    private EditText etStartTime;
    private EditText etDateTo;
    private EditText etTimeTo;
    private EditText etExpense;
    private EditText etDescription;
    private Spinner spinDay;
    private ImageView ivHeading;
    private LinearLayout llFormMore;
    private LinearLayout llFormMoreExtra;
    private LinearLayout llFormTransportType;
    private LinearLayout llHeadingHolder;
    private LinearLayout llMapLoc;
    /**
     * From inflated layouts
     */
    private EditText etConfirmationCode_acc;
    private EditText etHost;
    private EditText etConfirmationCode_tra;
    private EditText etTransportNumber;
    private EditText etAirline;
    private EditText etTerminal;
    private EditText etGate;
    private TextView tvTransportNumber;
    private Button btnTransportFlight;
    private Button btnTransportCar;
    private Button btnTransportOther;
    private LinearLayout llConfirmationCode_tra;
    private LinearLayout llAirline;
    private LinearLayout llTerminal;
    private LinearLayout llGate;
    private LinearLayout llTransportNumber;
    // ---
    private boolean isStartTimeClicked = false;
    private String transportationType = "flight";
    Validator validator;
    private String mapPlaceId = null;
    private String mapPlaceName = null;
    private String mapPlaceAddress = null;
    private String mapPlaceLat = null;
    private String mapPlaceLng = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        Bundle extras = getIntent().getExtras();
        String tripJson = extras.getString("TRIP_JSON");
        trip = new Gson().fromJson(tripJson, Trip.class);
        String eventTypeJson = extras.getString("EVENT_TYPE_JSON");
        eventType = new Gson().fromJson(eventTypeJson, EventType.class);
        Log.d("T", trip.getTitle());
        Log.d("ET", eventType.getType());

        validator = new Validator(this);

        initUI();
        setDaySpinner();
        setEventListeners();
        modifyUIForEventType();

        if (extras.containsKey("EVENT_EDIT_JSON")) {
            String editEventJson = extras.getString("EVENT_EDIT_JSON");
            editEvent = new Gson().fromJson(editEventJson, Event.class);
            editTripDayPos = extras.getInt("TRIP_DAY_POS");

            toolbar.setTitle("Edit Event");
            populateWithEvent();
        }
    }

    private void populateWithEvent() {
        etTitle.setText(editEvent.getTitle());

        if (editEvent.getFromTime() != null) {
            etStartTime.setText(DateUtil.formatDateString(editEvent.getFromTime(), DateUtil.DB_TIME_FORMAT, DateUtil.APP_TIME_FORMAT));
        }
        if (editEvent.getToTime() != null) {
            etTimeTo.setText(DateUtil.formatDateString(editEvent.getToTime(), DateUtil.DB_TIME_FORMAT, DateUtil.APP_TIME_FORMAT));
        }
        if (editEvent.getToDate() != null) {
            etDateTo.setText(DateUtil.formatDateString(editEvent.getToDate().toString(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_LONG));
        }

        etExpense.setText(editEvent.getExpenseAmount());
        etDescription.setText(editEvent.getDescription());
        spinDay.setSelection(editTripDayPos);

        if (editEvent.getGooglePlace() != null) {
            mapPlaceId = editEvent.getGooglePlace().getPlaceIdentifier();
            mapPlaceName = editEvent.getGooglePlace().getPlaceName();
            mapPlaceAddress = editEvent.getGooglePlace().getPlaceAddress();
            mapPlaceLat = String.valueOf(editEvent.getGooglePlace().getPlaceLat());
            mapPlaceLng = String.valueOf(editEvent.getGooglePlace().getPlaceLng());
            if (mapPlaceName != null) {
                tvMapLocName.setText(mapPlaceName);
                llMapLoc.setVisibility(View.VISIBLE);
            }
        }

        switch (eventType.getType()) {
            case "accommodation":
                etHost.setText(editEvent.getEventChild().getHost());
                etConfirmationCode_acc.setText(editEvent.getEventChild().getConfirmationCode());
                break;
            case "transportation":
                transportationType = editEvent.getEventChild().getTransportationType().getType();
                etConfirmationCode_tra.setText(editEvent.getEventChild().getConfirmationCode());
                etTransportNumber.setText(editEvent.getEventChild().getTransportNumber());
                etAirline.setText(editEvent.getEventChild().getAirline());
                etTerminal.setText(editEvent.getEventChild().getTerminal());
                etGate.setText(editEvent.getEventChild().getGate());

                if (transportationType.equals("flight")) selectTransportFlight();
                if (transportationType.equals("car")) selectTransportCar();
                if (transportationType.equals("other")) selectTransportOther();
                Log.i("TRANS_TYPE", transportationType);
                break;
        }
    }

    private void setDaySpinner() {
        ArrayList<String> spinnerArray = new ArrayList<String>();
        for (int i = 0; i < trip.getTripDays().size(); i++) {
            String dayTitle = "DAY " + (i + 1) + " - " +
                    DateUtil.convertDateToString(trip.getTripDays().get(i).getCalculatedDate(trip),
                            DateUtil.APP_DATE_FORMAT_SHORT);
            spinnerArray.add(dayTitle);
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        spinDay.setAdapter(spinnerArrayAdapter);
    }

    private void initUI() {
        clNewEvent = (CoordinatorLayout) findViewById(R.id.clNewEvent);
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Add New Event");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnAddMapLoc = (Button) findViewById(R.id.btnAddMapLoc);
        btnFormMore = (Button) findViewById(R.id.btnFormMore);
        btnSave = (Button) findViewById(R.id.btnSave);
        tvHeading = (TextView) findViewById(R.id.tvHeading);
        ivHeading = (ImageView) findViewById(R.id.ivHeading);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvStartsAt = (TextView) findViewById(R.id.tvStartsAt);
        tvEndsAt = (TextView) findViewById(R.id.tvEndsAt);
        tvExpense = (TextView) findViewById(R.id.tvExpense);
        tvCurrency = (TextView) findViewById(R.id.tvCurrency);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvMapLocName = (TextView) findViewById(R.id.tvMapLocName);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etStartTime = (EditText) findViewById(R.id.etStartTime);
        etDateTo = (EditText) findViewById(R.id.etDateTo);
        etTimeTo = (EditText) findViewById(R.id.etTimeTo);
        etExpense = (EditText) findViewById(R.id.etExpense);
        spinDay = (Spinner) findViewById(R.id.spinDay);
        etDescription = (EditText) findViewById(R.id.etDescription);
        llFormMore = (LinearLayout) findViewById(R.id.llFormMore);
        llFormMoreExtra = (LinearLayout) findViewById(R.id.llFormMoreExtra);
        llFormTransportType = (LinearLayout) findViewById(R.id.llFormTransportType);
        llHeadingHolder = (LinearLayout) findViewById(R.id.llHeadingHolder);
        llMapLoc = (LinearLayout) findViewById(R.id.llMapLoc);

        tvHeading.setText(eventType.getTypeName());
        ivHeading.setImageDrawable(eventType.getIcon(this));
        /*tvHeading.setTextColor(Color.parseColor(eventType.getColor()));
        ivHeading.setColorFilter(Color.parseColor(eventType.getColor()));*/
        etStartTime.setFocusable(false);
        etStartTime.setClickable(true);
        etTimeTo.setFocusable(false);
        etTimeTo.setClickable(true);
        etDateTo.setFocusable(false);
        etDateTo.setClickable(true);
    }

    private void setEventListeners() {
        btnFormMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llFormMore.getVisibility() == View.GONE) {
                    llFormMore.setVisibility(View.VISIBLE);
                }
                else {
                    llFormMore.setVisibility(View.GONE);
                }
            }
        });
        btnAddMapLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                                    .build(NewEventActivity.this);
                    startActivityForResult(intent, 1);
                } catch (GooglePlayServicesRepairableException e) {

                } catch (GooglePlayServicesNotAvailableException e) {

                }
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });
        etStartTime.setOnClickListener(this);
        etTimeTo.setOnClickListener(this);
        etDateTo.setOnClickListener(this);
        validator.setValidationListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                /*Log.i("GOOGLE", "PLACE ID:             " + place.getId());
                Log.i("GOOGLE", "PLACE NAME:           " + place.getName());
                Log.i("GOOGLE", "PLACE ADDRESS:        " + place.getAddress());
                Log.i("GOOGLE", "PLACE COORDS:         " + place.getLatLng());
                Log.i("GOOGLE", "PLACE PHONE:          " + place.getPhoneNumber());
                Log.i("GOOGLE", "PLACE RATING:         " + place.getRating());
                Log.i("GOOGLE", "PLACE PRICE_LEVEL:    " + place.getPriceLevel());
                Log.i("GOOGLE", "PLACE ATTRIBUTIONS:   " + place.getAttributions());
                Log.i("GOOGLE", "PLACE LOCALE:         " + place.getLocale());
                Log.i("GOOGLE", "PLACE WEBSITE:        " + place.getWebsiteUri());
                Log.i("GOOGLE", "PLACE VIEWPORT:       " + place.getViewport());
                Log.i("GOOGLE", "PLACE TYPES:          " + place.getPlaceTypes());*/
                setMapLocation(place.getId(), place.getName(), place.getAddress(), place.getLatLng());
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("GOOGLE", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void setMapLocation(String placeId, CharSequence placeName, CharSequence placeAddress, LatLng placeLatLng) {
        mapPlaceId = placeId;
        mapPlaceName = placeName.toString();
        mapPlaceAddress = placeAddress.toString();
        mapPlaceLat = String.valueOf(placeLatLng.latitude);
        mapPlaceLng = String.valueOf(placeLatLng.longitude);

        tvMapLocName.setText(placeName);
        llMapLoc.setVisibility(View.VISIBLE);
    }

    private void createOrUpdateEvent(@Nullable Event event) {
        String progressDialogTxt = "Saving your new event";
        int eventId = 0;
        if (event != null) {
            // save changes (edit)
            progressDialogTxt = "Saving your changes";
            eventId = event.getId();
        }
        progressDialog = ProgressDialog.show(this, "Just a moment", progressDialogTxt, true);
        String startTimeString = null;
        String timeToString = null;
        String dateToString = null;

        int tripDayPos = spinDay.getSelectedItemPosition();

        if (!ValidationUtil.isEmpty(etStartTime)) {
            startTimeString = DateUtil.formatDateString(etStartTime.getText().toString(), DateUtil.APP_TIME_FORMAT, DateUtil.DB_TIME_FORMAT);
        }
        if (!ValidationUtil.isEmpty(etTimeTo)) {
            timeToString = DateUtil.formatDateString(etTimeTo.getText().toString(), DateUtil.APP_TIME_FORMAT, DateUtil.DB_TIME_FORMAT);
        }
        if (!ValidationUtil.isEmpty(etDateTo)) {
            dateToString = DateUtil.formatDateString(etDateTo.getText().toString(), DateUtil.APP_DATE_FORMAT_LONG, DateUtil.DB_DATE_FORMAT);
        }

        EventRepository.postCreateOrEdit(User.getAuthUserApiToken(this), trip.getId(),
                trip.getTripDays().get(tripDayPos).getId(),
                eventId,
                eventType.getType(),
                ValidationUtil.getStringOrNull(etTitle),
                ValidationUtil.getStringOrNull(startTimeString),
                ValidationUtil.getStringOrNull(dateToString),
                ValidationUtil.getStringOrNull(timeToString),
                ValidationUtil.getStringOrNull(etExpense),
                ValidationUtil.getStringOrNull(etDescription),
                ValidationUtil.getStringOrNull(etConfirmationCode_acc),
                ValidationUtil.getStringOrNull(etHost),
                ValidationUtil.getStringOrNull(etConfirmationCode_tra),
                ValidationUtil.getStringOrNull(etTransportNumber),
                ValidationUtil.getStringOrNull(etAirline),
                ValidationUtil.getStringOrNull(etTerminal),
                ValidationUtil.getStringOrNull(etGate),
                transportationType,
                mapPlaceId,
                mapPlaceName,
                mapPlaceAddress,
                mapPlaceLat,
                mapPlaceLng,
                new EventResponse() {
                    @Override
                    public void onSuccess(Event event) {
                        Log.d("EVENT", event.getId() + "");
                        progressDialog.dismiss();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("EVENT_ID", event.getId());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }

                    @Override
                    public void onError(int status_code) {
                        Log.i("SC", status_code + "");
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        progressDialog.dismiss();
                    }
                });
    }

    @Override
    public void onValidationSucceeded() {
        if (editEvent != null) createOrUpdateEvent(editEvent);
        else createOrUpdateEvent(null);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void modifyUIForEventType() {
        LinearLayout formExtra = null;
        switch (eventType.getType()) {
            case "accommodation":
                etTitle.setHint("e.g. Getting to hostel and checking-in");
                tvStartsAt.setText("Check-in");
                tvEndsAt.setText("Check-out");
                formExtra = (LinearLayout) getLayoutInflater().inflate(R.layout.new_event_accommodation, llFormMoreExtra, false);
                etConfirmationCode_acc = (EditText) formExtra.findViewById(R.id.etConfirmationCode);
                etHost = (EditText) formExtra.findViewById(R.id.etHost);
                llFormMoreExtra.addView(formExtra);
                break;
            case "attraction":
                etTitle.setHint("e.g. Visit Palace of Culture");
                break;
            case "activity":
                etTitle.setHint("e.g. Cycling around the town");
                break;
            case "food_and_drink":
                etTitle.setHint("e.g. Eat pizza at Pizza House");
                break;
            case "shopping":
                etTitle.setHint("e.g. Buy some souvenirs");
                break;
            case "transportation":
                final String flightHint = "e.g. Flight from Zagreb to Doha";
                etTitle.setHint(flightHint);
                tvStartsAt.setText("Departure");
                tvEndsAt.setText("Arrival");
                formExtra = (LinearLayout) getLayoutInflater().inflate(R.layout.new_event_transportation, llFormMoreExtra, false);
                etConfirmationCode_tra = (EditText) formExtra.findViewById(R.id.etConfirmationCode);
                etTransportNumber = (EditText) formExtra.findViewById(R.id.etTransportNumber);
                etAirline = (EditText) formExtra.findViewById(R.id.etAirline);
                etTerminal = (EditText) formExtra.findViewById(R.id.etTerminal);
                etGate = (EditText) formExtra.findViewById(R.id.etGate);
                tvTransportNumber = (TextView) formExtra.findViewById(R.id.tvTransportNumber);
                llTransportNumber = (LinearLayout) formExtra.findViewById(R.id.llTransportNumber);
                llAirline = (LinearLayout) formExtra.findViewById(R.id.llAirline);
                llTerminal = (LinearLayout) formExtra.findViewById(R.id.llTerminal);
                llGate = (LinearLayout) formExtra.findViewById(R.id.llGate);
                llConfirmationCode_tra = (LinearLayout) formExtra.findViewById(R.id.llConfirmationCode);
                llFormMoreExtra.addView(formExtra);
                llFormTransportType.setVisibility(View.VISIBLE);
                btnTransportFlight = (Button) findViewById(R.id.btnTransportFlight);
                btnTransportCar = (Button) findViewById(R.id.btnTransportCar);
                btnTransportOther = (Button) findViewById(R.id.btnTransportOther);

                btnTransportFlight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectTransportFlight();
                    }
                });
                btnTransportCar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectTransportCar();
                    }
                });
                btnTransportOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        selectTransportOther();
                    }
                });
                break;
        }
    }

    private void selectTransportOther() {
        resetTransportBtnColors();
        setActiveTransportBtn(btnTransportOther);
        etTitle.setHint("e.g. Bus from Spain to Portugal");
        llFormMoreExtra.setVisibility(View.VISIBLE);
        tvTransportNumber.setText("Transport number");
        tvTransportNumber.setHint("Transport number");
        llAirline.setVisibility(View.GONE);
        llTerminal.setVisibility(View.GONE);
        llGate.setVisibility(View.GONE);
        transportationType = "other";
    }

    private void selectTransportCar() {
        resetTransportBtnColors();
        setActiveTransportBtn(btnTransportCar);
        etTitle.setHint("e.g. Drive from Osijek to Budapest");
        llFormMoreExtra.setVisibility(View.GONE);
        transportationType = "car";
    }

    private void selectTransportFlight() {
        resetTransportBtnColors();
        setActiveTransportBtn(btnTransportFlight);
        etTitle.setHint("e.g. Flight from Zagreb to Doha");
        llFormMoreExtra.setVisibility(View.VISIBLE);
        tvTransportNumber.setText("Flight number");
        tvTransportNumber.setHint("Flight number");
        llAirline.setVisibility(View.VISIBLE);
        llTerminal.setVisibility(View.VISIBLE);
        llGate.setVisibility(View.VISIBLE);
        transportationType = "flight";
    }

    private void resetTransportBtnColors() {
        btnTransportCar.setBackgroundColor(0);
        btnTransportCar.setTextColor(getResources().getColor(R.color.colorPrimary));
        btnTransportFlight.setBackgroundColor(0);
        btnTransportFlight.setTextColor(getResources().getColor(R.color.colorPrimary));
        btnTransportOther.setBackgroundColor(0);
        btnTransportOther.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void setActiveTransportBtn(Button btn) {
        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btn.setTextColor(getResources().getColor(R.color.colorTextLight));
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String dateString = (monthOfYear + 1) + "-" + dayOfMonth + "-" + year;
        String dateStringFormat = DateUtil.formatDateString(dateString, "MM-dd-yyyy", DateUtil.APP_DATE_FORMAT_LONG);
        etDateTo.setText(dateStringFormat);
    }


    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String timeString = hourOfDay + ":" + minute;
        String timeStringFormat = DateUtil.formatDateString(timeString, "HH:mm", DateUtil.APP_TIME_FORMAT);
        if (isStartTimeClicked == true) {
            etStartTime.setText(timeStringFormat);
        }
        else {
            etTimeTo.setText(timeStringFormat);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                NewEventActivity.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                NewEventActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        switch (view.getId()) {
            case R.id.etStartTime:
                isStartTimeClicked = true;
                tpd.show(getFragmentManager(), "timepicker_newEvent");
                break;
            case R.id.etTimeTo:
                isStartTimeClicked = false;
                tpd.show(getFragmentManager(), "timepicker_newEvent");
                break;
            case R.id.etDateTo:
                dpd.show(getFragmentManager(), "datepicker_newEvent");
                break;
            default:
                break;
        }
    }
}
