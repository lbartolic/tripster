package com.example.asus.tripplanner.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.activities.EditDayActivity;
import com.example.asus.tripplanner.activities.HomeActivity;
import com.example.asus.tripplanner.activities.NewTripActivity;
import com.example.asus.tripplanner.activities.TripActivity;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.JsonParseUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by asus on 16.08.16..
 */
public class TripsListViewAdapter extends BaseAdapter {
    private Context context;
    private List<Trip> tripList;

    public TripsListViewAdapter(Context context, List<Trip> tripList) {
        super();
        this.context = context;
        this.tripList = tripList;
    }

    @Override
    public int getCount() {
        return this.tripList.size();
    }

    @Override
    public Object getItem(int i) {
        return tripList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.list_item_trips, null);
        }
        final int position = i;

        TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        TextView tvStartDate = (TextView) view.findViewById(R.id.tvStartDate);
        TextView tvEndDate = (TextView) view.findViewById(R.id.tvEndDate);
        TextView tvCreatedBy = (TextView) view.findViewById(R.id.tvCreatedBy);
        TextView tvDaysNum = (TextView) view.findViewById(R.id.tvDaysNum);
        TextView tvPeopleNum = (TextView) view.findViewById(R.id.tvPeopleNum);
        FloatingActionButton fabOpen = (FloatingActionButton) view.findViewById(R.id.fabOpen);
        FloatingActionButton fabExtra = (FloatingActionButton) view.findViewById(R.id.fabExtra);

        Trip currentTrip = tripList.get(i);
        tvTitle.setText(currentTrip.getTitle());
        tvDescription.setText(currentTrip.getDescription());
        tvStartDate.setText(DateUtil.formatDateString(currentTrip.getStartDate(), DateUtil.DB_TIMESTAMP_FORMAT, DateUtil.APP_DATE_FORMAT_LONG));
        if (currentTrip.getTripDays().size() == 0 || currentTrip.getTripDays() == null) {
            tvEndDate.setText("?");
        }
        else {
            Date lastDayDate = currentTrip.getTripDays()
                    .get(currentTrip.getTripDays().size() - 1).getCalculatedDate(currentTrip);
            tvEndDate.setText(DateUtil.convertDateToString(lastDayDate, DateUtil.APP_DATE_FORMAT_LONG));
        }
        tvCreatedBy.setText(currentTrip.getUser().getFirstName() + " " + currentTrip.getUser().getLastName());
        tvDaysNum.setText(String.valueOf(currentTrip.getTripDays().size()));
        tvPeopleNum.setText(String.valueOf(currentTrip.getUsers().size()));

        /*usersHolder.removeAllViews();
        for (int index = 0; index < currentTrip.getUsers().size(); index++) {
            LinearLayout userPartial = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.partial_user, usersHolder, false);
            ((TextView) userPartial.findViewById(R.id.tvUser)).setText(currentTrip.getUsers().get(index).getFirstName());
            usersHolder.addView(userPartial);
        }*/

        fabOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TripActivity.class);
                intent.putExtra("TRIP_ID", tripList.get(position).getId());
                ((HomeActivity) context).startActivityForResult(intent, 1);
            }
        });

        fabExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewTripActivity.class);
                intent.putExtra("TRIP_EDIT_JSON", JsonParseUtil.convertObjectToJson(tripList.get(position)));
                ((HomeActivity) context).startActivityForResult(intent, 1);
            }
        });

        return view;
    }
}
