package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.GooglePlace;

import java.util.List;

/**
 * Created by asus on 17.08.16..
 */
public interface PlaceListResponse {
    void onSuccess(List<GooglePlace> googlePlaces);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
