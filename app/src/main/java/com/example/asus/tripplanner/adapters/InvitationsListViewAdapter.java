package com.example.asus.tripplanner.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.activities.InvitationsActivity;
import com.example.asus.tripplanner.activities.TripActivity;
import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.InvitationRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by asus on 16.08.16..
 */
public class InvitationsListViewAdapter extends BaseAdapter {
    private Context context;
    private List<Invitation> invitationList;
    private String type;
    private Dialog dialogConfirm;
    private ProgressDialog progressDialog;

    public InvitationsListViewAdapter(Context context, List<Invitation> invitationList, String type) {
        super();
        this.context = context;
        this.invitationList = invitationList;
        this.type = type;
    }

    @Override
    public int getCount() {
        return this.invitationList.size();
    }
    @Override
    public Object getItem(int i) {
        return invitationList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.list_item_invitation, null);
        }
        final int position = i;

        TextView tvUser = (TextView) view.findViewById(R.id.tvUser);
        TextView tvTripTitle = (TextView) view.findViewById(R.id.tvTripTitle);
        TextView tvInvitationText = (TextView) view.findViewById(R.id.tvInvitationText);
        TextView tvStatusExtra = (TextView) view.findViewById(R.id.tvStatusExtra);
        LinearLayout llBtnsHolder = (LinearLayout) view.findViewById(R.id.llBtnsHolder);
        Button btn1 = (Button) view.findViewById(R.id.btn1);
        Button btn2 = (Button) view.findViewById(R.id.btn2);
        RelativeLayout rlHolder = (RelativeLayout) view.findViewById(R.id.rlHolder);

        dialogConfirm = new Dialog(context);
        dialogConfirm.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogConfirm.setContentView(R.layout.dialog_confirm);
        (dialogConfirm.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogConfirm.dismiss();
            }
        });


        String invDateString = invitationList.get(position).getCreatedAt();
        String invDateFormated = DateUtil.formatDateString(invDateString, DateUtil.DB_TIMESTAMP_FORMAT,
                DateUtil.APP_DATE_FORMAT_DATE);
        tvTripTitle.setText(invitationList.get(position).getTrip().getTitle());

        if (type.equals("RECEIVED")) {
            if (invitationList.get(position).getStatus() == 0) {
                rlHolder.setBackgroundColor(context.getResources().getColor(R.color.colorAccentLighter));
            }

            tvUser.setText(invitationList.get(position).getSender().getFullName());
            tvInvitationText.setText("Invitation received on " + invDateFormated);

            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((TextView) dialogConfirm.findViewById(R.id.tvConfirmText))
                            .setText("Reject invitation for " +
                                    invitationList.get(position).getTrip().getTitle() +
                                    " trip");
                    (dialogConfirm.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            rejectInvitation(invitationList.get(position).getId());
                            dialogConfirm.dismiss();
                        }
                    });
                    dialogConfirm.show();
                }
            });
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    acceptInvitation(invitationList.get(position).getId());
                }
            });
        }
        else {
            tvUser.setText(invitationList.get(position).getReceiver().getFullName());
            tvInvitationText.setText("Invitation sent on " + invDateFormated);

            btn2.setVisibility(View.GONE);
            btn1.setText("Remove");
            btn1.setVisibility(View.VISIBLE);
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((TextView) dialogConfirm.findViewById(R.id.tvConfirmText))
                            .setText("Remove invitation to join trip planning for " +
                                    invitationList.get(position).getReceiver().getFullName());
                    (dialogConfirm.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeSentInvitation(invitationList.get(position).getId(), position);
                            dialogConfirm.dismiss();
                        }
                    });
                    dialogConfirm.show();
                }
            });
        }

        int invitationStatus = invitationList.get(position).getStatus();
        if (invitationStatus == 0) {
            llBtnsHolder.setVisibility(View.VISIBLE);
        }
        else {
            llBtnsHolder.setVisibility(View.GONE);
            tvStatusExtra.setVisibility(View.VISIBLE);
            if (invitationStatus == 1) {
                tvStatusExtra.setText("Accepted");
                tvStatusExtra.setTextColor(context.getResources().getColor(R.color.enabled));
            }
            if (invitationStatus == 2) {
                tvStatusExtra.setText("Rejected");
                tvStatusExtra.setTextColor(context.getResources().getColor(R.color.disabled));
            }
        }

        return view;
    }

    private void rejectInvitation(int id) {
        progressDialog = ProgressDialog.show(context, "Just a moment", "Please wait", true);
        InvitationRepository.postReject(User.getAuthUserApiToken(context), id, new InvitationResponse() {
            @Override
            public void onSuccess(Invitation invitation) {
                progressDialog.dismiss();
                ((InvitationsActivity) context).refreshData(null, true);
            }

            @Override
            public void onError(int status_code, String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void acceptInvitation(int id) {
        progressDialog = ProgressDialog.show(context, "Just a moment", "Please wait", true);
        InvitationRepository.postAccept(User.getAuthUserApiToken(context), id, new InvitationResponse() {
            @Override
            public void onSuccess(Invitation invitation) {
                progressDialog.dismiss();
                ((InvitationsActivity) context).refreshData(null, true);
            }

            @Override
            public void onError(int status_code, String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void removeSentInvitation(int id, final int position) {
        progressDialog = ProgressDialog.show(context, "Just a moment", "Please wait", true);
        InvitationRepository.postRemoveSent(User.getAuthUserApiToken(context), id, new InvitationResponse() {
            @Override
            public void onSuccess(Invitation invitation) {
                progressDialog.dismiss();
                invitationList.remove(position);
                InvitationsListViewAdapter.this.notifyDataSetChanged();
            }

            @Override
            public void onError(int status_code, String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }
}
