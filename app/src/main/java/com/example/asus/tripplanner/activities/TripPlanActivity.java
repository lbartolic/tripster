package com.example.asus.tripplanner.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.adapters.DayPagerAdapter;
import com.example.asus.tripplanner.adapters.TripTypePagerAdapter;
import com.example.asus.tripplanner.adapters.TripsListViewAdapter;
import com.example.asus.tripplanner.fragments.TripDayFragment;
import com.example.asus.tripplanner.models.EventType;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.utils.ListUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventTypeListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.EventTypeRepository;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.gson.Gson;

import java.util.Date;
import java.util.List;

import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

public class TripPlanActivity extends AppCompatActivity {
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private TabLayout tlDay;
    private ViewPager dayPager;
    private PagerAdapter pagerAdapter;
    private TextView tvTripTitle;
    private Button btnNewDay;
    private FloatingActionButton fabNewEvent;
    private Trip trip;
    private ProgressBar pbTripPlan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_plan);

        Bundle extras = getIntent().getExtras();
        String tripJson = extras.getString("TRIP_JSON");
        trip = new Gson().fromJson(tripJson, Trip.class);
        initUI();
        setEventListeners();
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Trip Days and Events");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnNewDay = (Button) findViewById(R.id.btnNewDay);
        pbTripPlan = (ProgressBar) findViewById(R.id.pbTripPlan);

        fabNewEvent = (FloatingActionButton) findViewById(R.id.fabNewEvent);
        tvTripTitle = (TextView) findViewById(R.id.tvTripTitle);
        tvTripTitle.setText(trip.getTitle());
        tlDay = (TabLayout) findViewById(R.id.tlTop);
        dayPager = (ViewPager) findViewById(R.id.viewPager);
        pagerAdapter = new DayPagerAdapter(getSupportFragmentManager(), this, trip);
        dayPager.setAdapter(pagerAdapter);
        dayPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlDay));
        tlDay.setupWithViewPager(dayPager);
        tlDay.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                dayPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        tlDay.setTabGravity(TabLayout.GRAVITY_FILL);
        tlDay.setupWithViewPager(dayPager);
    }

    private void setEventListeners() {
        fabNewEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(TripPlanActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_new_event_type);
                final ProgressBar pbEventTypes = (ProgressBar) dialog.findViewById(R.id.pbEventTypes);
                pbEventTypes.setVisibility(View.VISIBLE);

                EventTypeRepository.getEventTypes(User.getAuthUserApiToken(TripPlanActivity.this), new EventTypeListResponse() {
                    @Override
                    public void onSuccess(final List<EventType> eventTypeList) {
                        LinearLayout llBtnsHolder = null;
                        for (int i = 0; i < eventTypeList.size(); i++) {
                            final int index = i;
                            if (i == 0) llBtnsHolder = (LinearLayout) dialog.findViewById(R.id.llFirstRow);
                            if (i == 2) llBtnsHolder = (LinearLayout) dialog.findViewById(R.id.llSecondRow);
                            if (i == 4) llBtnsHolder = (LinearLayout) dialog.findViewById(R.id.llThirdRow);
                            if (i == 6) llBtnsHolder = (LinearLayout) dialog.findViewById(R.id.llFourthRow);

                            final Button eventTypeButton = (Button) getLayoutInflater().inflate(R.layout.new_event_type_dialog_btn, llBtnsHolder, false);
                            eventTypeButton.setText(eventTypeList.get(i).getTypeName());
                            eventTypeButton.setBackgroundColor(Color.parseColor(eventTypeList.get(i).getColor()));
                            eventTypeButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(TripPlanActivity.this, NewEventActivity.class);
                                    intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                                    intent.putExtra("EVENT_TYPE_JSON", JsonParseUtil.convertObjectToJson(eventTypeList.get(index)));
                                    startActivityForResult(intent, 2);
                                }
                            });
                            llBtnsHolder.addView(eventTypeButton);

                            Animation fadeInAnimation = AnimationUtils.loadAnimation(TripPlanActivity.this, R.anim.fade_in);
                            llBtnsHolder.startAnimation(fadeInAnimation);
                        }
                        (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        pbEventTypes.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(int status_code) {

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });

                (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        btnNewDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TripPlanActivity.this, NewDayActivity.class);
                intent.putExtra("TRIP_ID", trip.getId());
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                int result = data.getIntExtra("TRIP_DAY_ID", -1);
                refreshData(trip, null, true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){
                int result = data.getIntExtra("EVENT_ID", -1);
                refreshData(trip, null, true);
                Log.i("asd", "asdasd");
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    public void refreshData(Trip trip, @Nullable final TripDayFragment tdf, final boolean showFragmentLoading) {
        final int currentItem = dayPager.getCurrentItem();
        if (showFragmentLoading == true) {
            showFragmentLoading();
        }
        TripRepository.getTrip(User.getAuthUserApiToken(this), trip.getId(), new TripResponse() {
            @Override
            public void onSuccess(Trip trip) {
                TripPlanActivity.this.trip = trip;
                pagerAdapter = new DayPagerAdapter(getSupportFragmentManager(), TripPlanActivity.this, trip);
                dayPager.setAdapter(pagerAdapter);
                tlDay.setupWithViewPager(dayPager);
                dayPager.setCurrentItem(currentItem);
                if (tdf != null) {
                    tdf.stopSwipeRefresh();
                }
                if (showFragmentLoading == true) {
                    hideFragmentLoading();
                }
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void hideFragmentLoading() {
        pbTripPlan.setVisibility(View.GONE);
        dayPager.setVisibility(View.VISIBLE);
    }

    private void showFragmentLoading() {
        dayPager.setVisibility(View.GONE);
        pbTripPlan.setVisibility(View.VISIBLE);
    }
}
