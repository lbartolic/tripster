package com.example.asus.tripplanner.webservices.TripPlanner.repositories;

import android.util.Log;

import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.GooglePlace;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.PreferencesUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.TripPlannerClient;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventTypeListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.PlaceListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.PlaceResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripDayResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asus on 17.08.16..
 */
public class TripRepository {
    private static final TripInterface tripInterface = TripPlannerClient.getClient()
            .create(TripInterface.class);

    public TripRepository() {

    }

    public static void postCreateOrUpdate(String api_key, int tripId, String title, String description, String startDate, String currency, final TripResponse tripResponse) {
        Call<Trip> call = tripInterface.createOrUpdate(api_key, tripId, title, description, startDate, currency);
        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(Call<Trip> call, Response<Trip> response) {
                if (response.isSuccessful()) {
                    Trip trip = response.body();
                    tripResponse.onSuccess(trip);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    tripResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<Trip> call, Throwable t) {
                tripResponse.onFailure(t);
            }
        });
    }

    public static void postLeaveTrip(String api_key, int tripId, final TripResponse tripResponse) {
        Call<Trip> call = tripInterface.leaveTrip(api_key, tripId);
        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(Call<Trip> call, Response<Trip> response) {
                if (response.isSuccessful()) {
                    Trip trip = response.body();
                    tripResponse.onSuccess(trip);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    tripResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<Trip> call, Throwable t) {
                tripResponse.onFailure(t);
            }
        });
    }

    public static void postCreateTripDay(String api_key, int tripId, String title, final TripDayResponse tripDayResponse) {
        Call<TripDay> call = tripInterface.createTripDay(api_key, tripId, title);
        call.enqueue(new Callback<TripDay>() {
            @Override
            public void onResponse(Call<TripDay> call, Response<TripDay> response) {
                if (response.isSuccessful()) {
                    TripDay tripDay = response.body();
                    tripDayResponse.onSuccess(tripDay);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    tripDayResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<TripDay> call, Throwable t) {
                tripDayResponse.onFailure(t);
            }
        });
    }

    public static void putUpdateTripDay(String api_key, int tripDayId, String title, final TripDayResponse tripDayResponse) {
        Call<TripDay> call = tripInterface.updateTripDay(api_key, tripDayId, title);
        call.enqueue(new Callback<TripDay>() {
            @Override
            public void onResponse(Call<TripDay> call, Response<TripDay> response) {
                if (response.isSuccessful()) {
                    TripDay tripDay = response.body();
                    tripDayResponse.onSuccess(tripDay);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    tripDayResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<TripDay> call, Throwable t) {
                tripDayResponse.onFailure(t);
            }
        });
    }

    public static void deleteDestroyTripDay(String api_key, int tripDayId, final TripDayResponse tripDayResponse) {
        Call<TripDay> call = tripInterface.destroyTripDay(api_key, tripDayId);
        call.enqueue(new Callback<TripDay>() {
            @Override
            public void onResponse(Call<TripDay> call, Response<TripDay> response) {
                if (response.isSuccessful()) {
                    TripDay tripDay = response.body();
                    tripDayResponse.onSuccess(tripDay);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    Log.d("MSG", response.code() + "");
                    tripDayResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<TripDay> call, Throwable t) {
                tripDayResponse.onFailure(t);
            }
        });
    }

    public static void getTrips(String api_key, final TripListResponse tripResponse) {
        Call<List<Trip>> call = tripInterface.trips(api_key);
        call.enqueue(new Callback<List<Trip>>() {
            @Override
            public void onResponse(Call<List<Trip>> call, Response<List<Trip>> response) {
                if (response.isSuccessful()) {
                    List<Trip> tripList = response.body();
                    tripResponse.onSuccess(tripList);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    tripResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<List<Trip>> call, Throwable t) {
                tripResponse.onFailure(t);
            }
        });
    }

    public static void getTrip(String api_key, int id, final TripResponse tripResponse) {
        Call<Trip> call = tripInterface.trip(api_key, id);
        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(Call<Trip> call, Response<Trip> response) {
                if (response.isSuccessful()) {
                    Trip trip = response.body();
                    //Log.d("TRIP_EVENT", trip.getTripDays().get(0).getEvents().get(0).getChildJson().get("confirmation_code").toString());
                    tripResponse.onSuccess(trip);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    tripResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<Trip> call, Throwable t) {
                tripResponse.onFailure(t);
            }
        });
    }

    public static void getTripPlaces(String api_key, int tripId, final PlaceListResponse placeListResponse) {
        Call<List<GooglePlace>> call = tripInterface.tripPlaces(api_key, tripId);
        call.enqueue(new Callback<List<GooglePlace>>() {
            @Override
            public void onResponse(Call<List<GooglePlace>> call, Response<List<GooglePlace>> response) {
                if (response.isSuccessful()) {
                    List<GooglePlace> googlePlaces = response.body();
                    placeListResponse.onSuccess(googlePlaces);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    placeListResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<List<GooglePlace>> call, Throwable t) {
                placeListResponse.onFailure(t);
            }
        });
    }

    public static void getTripExpenses(String api_key, int tripId, final EventListResponse eventListResponse) {
        Call<List<Event>> call = tripInterface.tripExpenses(api_key, tripId);
        call.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                if (response.isSuccessful()) {
                    List<Event> events = response.body();
                    eventListResponse.onSuccess(events);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    eventListResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                eventListResponse.onFailure(t);
            }
        });
    }
}
