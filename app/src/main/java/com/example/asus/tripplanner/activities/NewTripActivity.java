package com.example.asus.tripplanner.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.PreferencesUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewTripActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, Validator.ValidationListener {
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private LinearLayout llTitle;
    private LinearLayout llDescription;
    private LinearLayout llStartDate;
    @NotEmpty
    private EditText etTitle;
    private EditText etDescription;
    @NotEmpty
    private EditText etStartDate;
    @NotEmpty
    private EditText etCurrency;
    private Button btnSave;
    private Validator validator;
    private Trip editTrip;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);

        validator = new Validator(this);

        initUI();
        setEventListeners();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey("TRIP_EDIT_JSON")) {
                // edit Trip details if exists
                String editTripJson = extras.getString("TRIP_EDIT_JSON");
                editTrip = new Gson().fromJson(editTripJson, Trip.class);

                toolbar.setTitle("Edit Trip");
                populateWithTrip();
            }
        }
    }

    private void populateWithTrip() {
        etTitle.setText(editTrip.getTitle());
        etDescription.setText(editTrip.getDescription());
        etCurrency.setText(editTrip.getExpenseCurrency());
        etStartDate.setText(DateUtil.formatDateString(editTrip.getStartDate(), DateUtil.DB_TIMESTAMP_FORMAT, DateUtil.APP_DATE_FORMAT_LONG));
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Plan New Trip");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        llTitle = (LinearLayout) findViewById(R.id.llTitle);
        llDescription = (LinearLayout) findViewById(R.id.llDescription);
        llStartDate = (LinearLayout) findViewById(R.id.llStartDate);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etDescription = (EditText) findViewById(R.id.etDescription);
        etStartDate = (EditText) findViewById(R.id.etStartDate);
        etCurrency = (EditText) findViewById(R.id.etCurrency);
        etStartDate.setFocusable(false);
        etStartDate.setClickable(true);
        btnSave = (Button) findViewById(R.id.btnSave);
    }

    private void setEventListeners() {
        llTitle.setOnClickListener(this);
        llDescription.setOnClickListener(this);
        llStartDate.setOnClickListener(this);
        etStartDate.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int month, int day) {
        String dateString = (month+1) + "-" + day + "-" + year;
        etStartDate.setText(DateUtil.formatDateString(dateString, "MM-dd-yyyy", DateUtil.APP_DATE_FORMAT_LONG));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        switch (view.getId()) {
            case R.id.llTitle:
                etTitle.requestFocus();
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                break;
            case R.id.llDescription:
                etDescription.requestFocus();
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                break;
            case R.id.llStartDate:
            case R.id.etStartDate:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        NewTripActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(), "datepicker_newTrip");
                break;
            case R.id.btnSave:
                validator.validate();
            default:
                break;
        }
    }

    private void createOrUpdateTrip(@Nullable final Trip editTrip) {
        progressDialog = ProgressDialog.show(this, "Just a moment", "Saving trip details", true);
        int tripId = 0;
        if (editTrip != null) {
            tripId = editTrip.getId();
        }

        String title = etTitle.getText().toString();
        String description = etDescription.getText().toString();
        String currency = etCurrency.getText().toString();
        String dateString = etStartDate.getText().toString();
        String dateTimestamp = DateUtil.formatDateString(dateString, DateUtil.APP_DATE_FORMAT_LONG, DateUtil.DB_TIMESTAMP_FORMAT);
        TripRepository.postCreateOrUpdate(User.getAuthUserApiToken(this), tripId, title, description, dateTimestamp, currency, new TripResponse() {
            @Override
            public void onSuccess(Trip trip) {
                progressDialog.dismiss();
                if (editTrip != null) {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else {
                    Log.d("TRIP", "onSuccess");
                    Intent intent;
                    intent = new Intent(NewTripActivity.this, TripActivity.class);
                    intent.putExtra("TRIP_ID", trip.getId());
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onError(int status_code) {
                Log.d("TRIP", status_code + "");
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("TRIP", throwable + "");
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        if (editTrip != null) createOrUpdateTrip(editTrip);
        else createOrUpdateTrip(null);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
