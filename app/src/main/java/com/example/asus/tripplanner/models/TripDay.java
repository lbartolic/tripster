package com.example.asus.tripplanner.models;

import com.example.asus.tripplanner.utils.DateUtil;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by asus on 16.08.16..
 */
public class TripDay {
    private static final String PREFS_KEY = "trip_days";

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("trip")
    private Trip trip;
    @SerializedName("events")
    private List<Event> events;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Trip getTrip() {
        return trip;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public List<Event> getEvents() {
        return events;
    }

    public Date getCalculatedDate(Trip trip) {
        int offset = 0;
        Date tripDayDate = null;
        String tripStartDateString = trip.getStartDate();
        Date tripStartDate = DateUtil.convertStringToDate(tripStartDateString, DateUtil.DB_TIMESTAMP_FORMAT);
        Calendar cal = Calendar.getInstance();
        cal.setTime(tripStartDate);
        for (int i = 0; i < trip.getTripDays().size(); i++) {
            if (trip.getTripDays().get(i).getId() == this.getId()) {
                cal.add(Calendar.DATE, offset);
                tripDayDate = cal.getTime();
            }
            else {
                offset++;
            }
        }
        return tripDayDate;
    }
}
