package com.example.asus.tripplanner.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by asus on 13.08.16..
 */
public class PreferencesUtil {
    private static final String PREFERENCE_KEY = "trip_planner";

    public static SharedPreferences getSharedPreferences(final Context context) {
        return context.getSharedPreferences(PREFERENCE_KEY, context.MODE_PRIVATE);
    }


}
