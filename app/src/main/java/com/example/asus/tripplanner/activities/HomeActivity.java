package com.example.asus.tripplanner.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.adapters.TripTypePagerAdapter;
import com.example.asus.tripplanner.adapters.TripsListViewAdapter;
import com.example.asus.tripplanner.fragments.PastTripsFragment;
import com.example.asus.tripplanner.fragments.UpcomingTripsFragment;
import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.InvitationRepository;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.mikepenz.actionitembadge.library.ActionItemBadge;

import org.w3c.dom.Text;

import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private List<Trip> tripList;
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private TabLayout tlTripType;
    private ViewPager tripTypePager;
    private PagerAdapter pagerAdapter;
    private Button btnNewTrip;
    private LinearLayout llHomeHolder;
    private LinearLayout llBtnNewTrip;
    private ProgressBar pbTrips;
    private int numOfNewInvitations = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initUI();
        setViewPager();
        setNewInvitations();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        setNewInvitations();
    }

    private void setNewInvitations() {
        InvitationRepository.getNewReceived(User.getAuthUserApiToken(this), new InvitationListResponse() {
            @Override
            public void onSuccess(List<Invitation> invitationList) {
                int count = invitationList.size();
                numOfNewInvitations = count;
                invalidateOptionsMenu();
            }

            @Override
            public void onError(int status_code, String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    public void refreshData(@Nullable final UpcomingTripsFragment tdf, @Nullable final PastTripsFragment ptf, final boolean showFragmentLoading) {
        final int currentItem = tripTypePager.getCurrentItem();
        if (showFragmentLoading == true) {
            showFragmentLoading();
        }
        TripRepository.getTrips(User.getAuthUserApiToken(this), new TripListResponse() {
            @Override
            public void onSuccess(List<Trip> tripList) {
                pagerAdapter = new TripTypePagerAdapter(getSupportFragmentManager(), HomeActivity.this, tripList);
                tripTypePager.setAdapter(pagerAdapter);
                tlTripType.setupWithViewPager(tripTypePager);
                tripTypePager.setCurrentItem(currentItem);
                setNewInvitations();
                if (tdf != null) tdf.stopSwipeRefresh();
                if (ptf != null) ptf.stopSwipeRefresh();
                if (showFragmentLoading == true) {
                    hideFragmentLoading();
                }
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void hideFragmentLoading() {
        pbTrips.setVisibility(View.GONE);
        tripTypePager.setVisibility(View.VISIBLE);
    }

    private void showFragmentLoading() {
        tripTypePager.setVisibility(View.GONE);
        pbTrips.setVisibility(View.VISIBLE);
    }

    private void setViewPager() {
        TripRepository.getTrips(User.getAuthUserApiToken(this), new TripListResponse() {
            @Override
            public void onSuccess(final List<Trip> tripList) {
                HomeActivity.this.tripList = tripList;
                pagerAdapter = new TripTypePagerAdapter(getSupportFragmentManager(), HomeActivity.this, tripList);
                tripTypePager.setAdapter(pagerAdapter);
                tripTypePager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlTripType));
                tlTripType.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        tripTypePager.setCurrentItem(tab.getPosition());
                        int tabIconColor = ContextCompat.getColor(HomeActivity.this, R.color.colorPrimary);
                        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
                tlTripType.setTabGravity(TabLayout.GRAVITY_FILL);
                tlTripType.setupWithViewPager(tripTypePager);
                pbTrips.setVisibility(View.GONE);
                tripTypePager.setVisibility(View.VISIBLE);
                llBtnNewTrip.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        btnNewTrip = (Button) findViewById(R.id.btnNewTrip);
        btnNewTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(HomeActivity.this, NewTripActivity.class);
                startActivity(intent);
            }
        });

        toolbar.setTitle("My Trips");
        setSupportActionBar(toolbar);
        initNavigationDrawer();

        llHomeHolder = (LinearLayout) findViewById(R.id.llHomeHolder);
        llBtnNewTrip = (LinearLayout) findViewById(R.id.llBtnNewTrip);
        tlTripType = (TabLayout) findViewById(R.id.tlTop);
        tripTypePager = (ViewPager) findViewById(R.id.viewPager);
        pbTrips = (ProgressBar) findViewById(R.id.pbTrips);
    }

    /**
     * Set up navigation drawer (vertical nav), attach it to toolbar,
     * add event listener for navigation drawer menu items.
     */
    private void initNavigationDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                Intent intent;
                switch (id) {
                    case R.id.navInvitations:
                        intent = new Intent(HomeActivity.this, InvitationsActivity.class);
                        startActivityForResult(intent, 2);
                        break;
                    case R.id.navLogOut:
                        logout();
                        break;
                }

                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        View navHeader = navigationView.getHeaderView(0);
        TextView navFullName = (TextView) navHeader.findViewById(R.id.navHeaderFullName);
        TextView navEmail = (TextView) navHeader.findViewById(R.id.navHeaderEmail);
        navFullName.setText(User.getAuthUserFullName(this));
        navEmail.setText(User.getAuthUserEmail(this));
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    private void logout() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        String msg = "Do you want to log out?";
        ((TextView) dialog.findViewById(R.id.tvConfirmText)).setText(msg);
        (dialog.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User.removeUserPrefs(HomeActivity.this);
                Intent intent;
                intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

    /**
     * Actions to do when user presses "back".
     */
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Attach menu items from .xml file to toolbar.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        ActionItemBadge.hide(menu.findItem(R.id.invitationNotif));
        if (numOfNewInvitations > 0) {
            ActionItemBadge.update(this, menu.findItem(R.id.invitationNotif),
                    getResources().getDrawable(R.drawable.ic_notifications_white_24dp1),
                    ActionItemBadge.BadgeStyles.RED_LARGE, numOfNewInvitations);
        }
        return true;
    }

    /**
     * Handle toolbar menu item clicks.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.invitationNotif) {
            Intent intent;
            intent = new Intent(HomeActivity.this, InvitationsActivity.class);
            startActivityForResult(intent, 2);
            ActionItemBadge.hide(item);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                refreshData(null, null, true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                refreshData(null, null, true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}
