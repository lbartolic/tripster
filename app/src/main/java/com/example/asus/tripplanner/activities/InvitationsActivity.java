package com.example.asus.tripplanner.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.adapters.InvitationTypePagerAdapter;
import com.example.asus.tripplanner.adapters.TripTypePagerAdapter;
import com.example.asus.tripplanner.fragments.InvitationTypeFragment;
import com.example.asus.tripplanner.fragments.PastTripsFragment;
import com.example.asus.tripplanner.fragments.UpcomingTripsFragment;
import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.InvitationRepository;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;

import java.util.List;

public class InvitationsActivity extends AppCompatActivity {
    private List<Invitation> invitationList;
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private TabLayout tlType;
    private ViewPager typePager;
    private PagerAdapter pagerAdapter;
    private ProgressBar pbInvitations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitations);

        initUI();
        setEventListeners();
        setViewPager();
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Invitations");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        pbInvitations = (ProgressBar) findViewById(R.id.pbInvitations);
        tlType = (TabLayout) findViewById(R.id.tlType);
        typePager = (ViewPager) findViewById(R.id.typePager);
    }

    private void setEventListeners() {

    }

    private void setViewPager() {
        InvitationRepository.getInvitations(User.getAuthUserApiToken(this), new InvitationListResponse() {
            @Override
            public void onSuccess(final List<Invitation> invitationList) {
                InvitationsActivity.this.invitationList = invitationList;
                pagerAdapter = new InvitationTypePagerAdapter(getSupportFragmentManager(), InvitationsActivity.this, invitationList);
                typePager.setAdapter(pagerAdapter);
                typePager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tlType));
                tlType.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        typePager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });
                tlType.setTabGravity(TabLayout.GRAVITY_FILL);
                tlType.setupWithViewPager(typePager);
                pbInvitations.setVisibility(View.GONE);
                typePager.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(int status_code, String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("INV", 0);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void refreshData(@Nullable final InvitationTypeFragment itf, final boolean showFragmentLoading) {
        final int currentItem = typePager.getCurrentItem();
        if (showFragmentLoading == true) {
            showFragmentLoading();
        }
        InvitationRepository.getInvitations(User.getAuthUserApiToken(this), new InvitationListResponse() {
            @Override
            public void onSuccess(List<Invitation> invitationList) {
                pagerAdapter = new InvitationTypePagerAdapter(getSupportFragmentManager(), InvitationsActivity.this, invitationList);
                typePager.setAdapter(pagerAdapter);
                tlType.setupWithViewPager(typePager);
                typePager.setCurrentItem(currentItem);
                if (itf != null) itf.stopSwipeRefresh();
                if (showFragmentLoading == true) {
                    hideFragmentLoading();
                }
            }

            @Override
            public void onError(int status_code, String message) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void hideFragmentLoading() {
        pbInvitations.setVisibility(View.GONE);
        typePager.setVisibility(View.VISIBLE);
    }

    private void showFragmentLoading() {
        typePager.setVisibility(View.GONE);
        pbInvitations.setVisibility(View.VISIBLE);
    }
}
