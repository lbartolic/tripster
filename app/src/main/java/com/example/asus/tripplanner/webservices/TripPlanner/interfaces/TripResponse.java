package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;

/**
 * Created by asus on 17.08.16..
 */
public interface TripResponse {
    void onSuccess(Trip trip);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
