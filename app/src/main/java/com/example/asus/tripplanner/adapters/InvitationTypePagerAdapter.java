package com.example.asus.tripplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.example.asus.tripplanner.fragments.InvitationTypeFragment;
import com.example.asus.tripplanner.fragments.TripDayFragment;
import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.JsonParseUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 15.08.16..
 */
public class InvitationTypePagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private List<Invitation> invitationList;
    private String tabTitles[] = new String[] {
            "RECEIVED", "SENT"
    };

    public InvitationTypePagerAdapter(FragmentManager fm, Context ctx, List<Invitation> invitationList) {
        super(fm);
        this.context = ctx;
        this.invitationList = invitationList;
    }

    @Override
    public Fragment getItem(int position) {
        List<Invitation> received = new ArrayList<>();
        List<Invitation> sent = new ArrayList<>();
        InvitationTypeFragment itf = new InvitationTypeFragment();
        for (int i = 0; i < invitationList.size(); i++) {
            Log.i("TOKEN", User.getAuthUserApiToken(context) + "");
            Log.i("TOKEN", invitationList.get(i).getReceiver().getApiToken() + "");
            if (invitationList.get(i).getReceiver().getApiToken().equals(User.getAuthUserApiToken(context))) {
                received.add(invitationList.get(i));
                Log.i("RECEIVED", invitationList.get(i).getId() + "");
            }
            else {
                sent.add(invitationList.get(i));
                Log.i("SENT", invitationList.get(i).getId() + "");
            }
        }
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                bundle.putString("INVITATIONS", JsonParseUtil.convertObjectToJson(received));
                bundle.putString("TYPE", "RECEIVED");
                break;
            case 1:
                bundle.putString("INVITATIONS", JsonParseUtil.convertObjectToJson(sent));
                bundle.putString("TYPE", "SENT");
                break;
        }
        itf.setArguments(bundle);
        return itf;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
