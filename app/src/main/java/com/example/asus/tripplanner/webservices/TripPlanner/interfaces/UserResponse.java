package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.User;

import retrofit2.Response;

/**
 * Created by asus on 14.08.16..
 */
public interface UserResponse {
    void onSuccess(User user);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
