package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by asus on 13.08.16..
 */
public interface InvitationInterface {
    @FormUrlEncoded
    @POST("{api_key}/invitations")
    Call<Invitation> create(@Path("api_key") String api_key, @Field("trip_id") int tripId, @Field("email") String email);

    @FormUrlEncoded
    @POST("{api_key}/invitations/reject")
    Call<Invitation> reject(@Path("api_key") String api_key, @Field("id") int invitationId);

    @FormUrlEncoded
    @POST("{api_key}/invitations/accept")
    Call<Invitation> accept(@Path("api_key") String api_key, @Field("id") int invitationId);

    @FormUrlEncoded
    @POST("{api_key}/invitations/removeSent")
    Call<Invitation> removeSent(@Path("api_key") String api_key, @Field("id") int invitationId);

    @GET("{api_key}/invitations")
    Call<List<Invitation>> invitations(@Path("api_key") String api_key);

    @GET("{api_key}/invitations/newReceived")
    Call<List<Invitation>> newReceived(@Path("api_key") String api_key);
}
