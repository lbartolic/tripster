package com.example.asus.tripplanner.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.UserRepository;

public class LoginActivity extends AppCompatActivity {
    private EditText etUsername;
    private EditText etPassword;
    private Button btnLogIn;
    private Button btnSignIn;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();
        initListeners();
    }

    private void initUI() {
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogIn = (Button) findViewById(R.id.btnLogIn);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
    }

    private void initListeners() {
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String[] credentials = {username, password};
                attemptUserAuth(credentials);
            }
        });
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void attemptUserAuth(String[] credentials) {
        progressDialog = ProgressDialog.show(this, null, "Authenticating", true);
        UserRepository.authenticateUser(credentials, new UserResponse() {
            @Override
            public void onSuccess(User user) {
                progressDialog.hide();
                String userJson = JsonParseUtil.convertObjectToJson(user);
                Log.d("RETROFIT_USER", userJson);
                handleSuccessfulAuth(user);
            }
            @Override
            public void onError(int code) {
                progressDialog.hide();
                showMessage();
            }
            @Override
            public void onFailure(Throwable t) {
                progressDialog.hide();
                showMessage();
            }
        });
    }

    private void showMessage() {
        final Dialog messageDialog = new Dialog(this);
        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(R.layout.dialog_message);
        Button btnClose = (Button) messageDialog.findViewById(R.id.btnClose);
        final TextView tvText = (TextView) messageDialog.findViewById(R.id.tvText);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        tvText.setText("Oops! Something's wrong. Please try again.");
        messageDialog.show();
    }

    private void handleSuccessfulAuth(User user) {
        User.setUserPrefs(this, user);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
