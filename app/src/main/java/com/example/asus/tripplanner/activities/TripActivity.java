package com.example.asus.tripplanner.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.InvitationRepository;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Date;
import java.util.List;

public class TripActivity extends AppCompatActivity implements Validator.ValidationListener {
    private int tripId;
    private Dialog dialogInvitation;
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private ScrollView scrollView;
    private ProgressBar pbTrip;
    private SwipeRefreshLayout srlTrip;
    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvStartDate;
    private TextView tvEndDate;
    private TextView tvDaysNum;
    private TextView tvEventsNum;
    private TextView tvPeopleNum;
    private LinearLayout llEventsByDay;
    private LinearLayout llUsersHolder;
    private Button btnPlanTrip;
    private Button btnMapLocations;
    private Button btnExpenses;
    private Button btnInvite;
    private Validator validator;
    private ProgressDialog progressDialog;
    @NotEmpty
    @Email
    private EditText etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip);

        Bundle extras = getIntent().getExtras();
        tripId = extras.getInt("TRIP_ID");
        initUI();
        setEventListeners();
        setTripDetails();
    }

    private void setTripDetails() {
        TripRepository.getTrip(User.getAuthUserApiToken(this), tripId, new TripResponse() {
            @Override
            public void onSuccess(final Trip trip) {
                tvTitle.setText(trip.getTitle());
                tvDescription.setText(trip.getDescription());
                tvStartDate.setText(DateUtil.formatDateString(trip.getStartDate(),
                        DateUtil.DB_TIMESTAMP_FORMAT, DateUtil.APP_DATE_FORMAT_LONG));
                if (trip.getTripDays().size() == 0 || trip.getTripDays() == null) {
                    tvEndDate.setText("?");
                }
                else {
                    Date lastDayDate = trip.getTripDays()
                            .get(trip.getTripDays().size() - 1).getCalculatedDate(trip);
                    tvEndDate.setText(DateUtil.convertDateToString(lastDayDate, DateUtil.APP_DATE_FORMAT_LONG));
                }
                int tripDaysNum = trip.getTripDays().size();
                tvDaysNum.setText(String.valueOf(tripDaysNum));
                /*int tripEventsNum = 0;
                for (int i = 0; i < trip.getTripDays().size(); i++) {
                    tripEventsNum += trip.getTripDays().get(i).getEvents().size();
                }*/
                tvEventsNum.setText(String.valueOf(trip.getEventsCount()));
                tvPeopleNum.setText(String.valueOf(trip.getUsers().size()));
                btnPlanTrip.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("asd", "asdasd");
                        Intent intent = new Intent(TripActivity.this, TripPlanActivity.class);
                        intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                        startActivity(intent);
                    }
                });
                btnMapLocations.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TripActivity.this, MapLocationsActivity.class);
                        intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                        startActivity(intent);
                    }
                });
                btnExpenses.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TripActivity.this, ExpensesActivity.class);
                        intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                        startActivity(intent);
                    }
                });
                btnInvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogInvitation = new Dialog(TripActivity.this);
                        dialogInvitation.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialogInvitation.setContentView(R.layout.dialog_send_invitation);
                        Button btnCancel = (Button) dialogInvitation.findViewById(R.id.btnCancel);
                        Button btnSend = (Button) dialogInvitation.findViewById(R.id.btnSend);
                        etEmail = (EditText) dialogInvitation.findViewById(R.id.etEmail);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialogInvitation.dismiss();
                            }
                        });
                        btnSend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                validator.validate();
                            }
                        });
                        dialogInvitation.show();
                    }
                });

                llEventsByDay.removeAllViews();
                for (int i = 0; i < trip.getTripDays().size(); i++) {
                    LinearLayout dayItem = (LinearLayout) getLayoutInflater().inflate(R.layout.events_by_day_item, llEventsByDay, false);
                    ((TextView) dayItem.findViewById(R.id.tvNumOfEvents)).setText(String.valueOf(trip.getTripDays().get(i).getEvents().size()));
                    ((TextView) dayItem.findViewById(R.id.tvDay)).setText("Day " + (i + 1));
                    llEventsByDay.addView(dayItem);
                }

                llUsersHolder.removeAllViews();
                for (int index = 0; index < trip.getUsers().size(); index++) {
                    LinearLayout userPartial = (LinearLayout) getLayoutInflater().inflate(R.layout.partial_user, llUsersHolder, false);
                    ((TextView) userPartial.findViewById(R.id.tvUser)).setText(trip.getUsers().get(index).getFirstName());
                    llUsersHolder.addView(userPartial);
                }

                stopSwipeRefresh();
                pbTrip.setVisibility(View.GONE);
                scrollView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    public void stopSwipeRefresh() {
        srlTrip.setRefreshing(false);
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Trip Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        scrollView = (ScrollView) findViewById(R.id.svTrip);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvEndDate = (TextView) findViewById(R.id.tvEndDate);
        tvDaysNum = (TextView) findViewById(R.id.tvDaysNum);
        tvEventsNum = (TextView) findViewById(R.id.tvEventsNum);
        tvPeopleNum = (TextView) findViewById(R.id.tvPeopleNum);
        llEventsByDay = (LinearLayout) findViewById(R.id.llEventsByDay);
        llUsersHolder = (LinearLayout) findViewById(R.id.llUsersHolder);
        btnPlanTrip = (Button) findViewById(R.id.btnPlanTrip);
        btnMapLocations = (Button) findViewById(R.id.btnMapLocations);
        btnExpenses = (Button) findViewById(R.id.btnExpenses);
        btnInvite = (Button) findViewById(R.id.btnInvite);
        pbTrip = (ProgressBar) findViewById(R.id.pbTrip);
        srlTrip = (SwipeRefreshLayout) findViewById(R.id.srlTrip);
        validator = new Validator(this);
    }

    private void setEventListeners() {
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();

                Log.d("Scroll", scrollY + "");
            }
        });
        srlTrip.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setTripDetails();
            }
        });
        validator.setValidationListener(this);
    }

    private void leaveTripGroup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        String msg = "If you leave this trip group, you can come back only if someone invites you. Leave this group?";
        ((TextView) dialog.findViewById(R.id.tvConfirmText)).setText(msg);
        (dialog.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                TripRepository.postLeaveTrip(User.getAuthUserApiToken(TripActivity.this), tripId, new TripResponse() {
                    @Override
                    public void onSuccess(Trip trip) {
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                    @Override
                    public void onError(int status_code) {

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_trip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.itemLeaveGroup:
                leaveTripGroup();
                break;
        }
        return true;
    }

    @Override
    public void onValidationSucceeded() {
        sendInvitation();
    }

    private void sendInvitation() {
        final Dialog messageDialog = new Dialog(TripActivity.this);
        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(R.layout.dialog_message);
        Button btnClose = (Button) messageDialog.findViewById(R.id.btnClose);
        final TextView tvText = (TextView) messageDialog.findViewById(R.id.tvText);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });

        progressDialog = ProgressDialog.show(this, "Just a moment", "Sending your invitation", true);
        InvitationRepository.postCreate(User.getAuthUserApiToken(this), tripId, etEmail.getText().toString(), new InvitationResponse() {
            @Override
            public void onSuccess(Invitation invitation) {
                progressDialog.dismiss();
                dialogInvitation.dismiss();
                tvText.setText("Your invitation has been sent to " +
                        invitation.getReceiver().getFirstName() + " " +
                        invitation.getReceiver().getLastName());
                messageDialog.show();
                Log.i("INV", invitation.getId() + "");
            }

            @Override
            public void onError(int status_code, String message) {
                progressDialog.dismiss();
                tvText.setText(message);
                messageDialog.show();
                Log.i("ERROR", status_code + " " + message);
            }

            @Override
            public void onFailure(Throwable throwable) {
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
