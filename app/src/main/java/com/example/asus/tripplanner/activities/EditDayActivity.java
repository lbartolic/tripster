package com.example.asus.tripplanner.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripDayResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.gson.Gson;

public class EditDayActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private EditText etTitle;
    private Button btnSave;
    private TripDay tripDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_day);
        Intent intent = getIntent();
        String tripDayJson = intent.getStringExtra("TRIP_DAY_JSON");
        tripDay = new Gson().fromJson(tripDayJson, TripDay.class);
        initUI();
        setEventListeners();
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Edit Trip Day");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        etTitle = (EditText) findViewById(R.id.etTitle);
        etTitle.setText(tripDay.getTitle());
        btnSave = (Button) findViewById(R.id.btnSave);
    }

    private void setEventListeners() {
        btnSave.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        switch (view.getId()) {
            case R.id.llTitle:
                etTitle.requestFocus();
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                break;
            case R.id.btnSave:
                updateDay();
            default:
                break;
        }
    }

    private void updateDay() {
        String title = etTitle.getText().toString();
        TripRepository.putUpdateTripDay(User.getAuthUserApiToken(this), tripDay.getId(), title, new TripDayResponse() {
            @Override
            public void onSuccess(TripDay tripDay) {
                Log.d("TRIP", "onSuccess");
                Log.d("TRIP", tripDay.getId() + "");
                Intent returnIntent = new Intent();
                returnIntent.putExtra("TRIP_DAY_ID", tripDay.getId());
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

            @Override
            public void onError(int status_code) {
                Log.d("TRIP_DAY", status_code + "");
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("TRIP_DAY", throwable + "");
            }
        });
    }
}
