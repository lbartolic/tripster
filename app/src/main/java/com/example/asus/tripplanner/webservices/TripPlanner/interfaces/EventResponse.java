package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.TripDay;

/**
 * Created by asus on 17.08.16..
 */
public interface EventResponse {
    void onSuccess(Event event);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
