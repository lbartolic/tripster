package com.example.asus.tripplanner.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripDayResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;

public class NewDayActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private LinearLayout llTitle;
    private EditText etTitle;
    private Button btnSave;
    private int tripId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_day);
        Intent intent = getIntent();
        tripId = intent.getIntExtra("TRIP_ID", 0);
        initUI();
        setEventListeners();
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Add New Day");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        llTitle = (LinearLayout) findViewById(R.id.llTitle);
        etTitle = (EditText) findViewById(R.id.etTitle);
        btnSave = (Button) findViewById(R.id.btnSave);
    }

    private void setEventListeners() {
        llTitle.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        switch (view.getId()) {
            case R.id.llTitle:
                etTitle.requestFocus();
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                break;
            case R.id.btnSave:
                createNewDay();
            default:
                break;
        }
    }

    private void createNewDay() {
        String title = etTitle.getText().toString();
        TripRepository.postCreateTripDay(User.getAuthUserApiToken(this), tripId, title, new TripDayResponse() {
            @Override
            public void onSuccess(TripDay tripDay) {
                Log.d("TRIP", "onSuccess");
                Log.d("TRIP", tripDay.getId() + "");
                Intent returnIntent = new Intent();
                returnIntent.putExtra("TRIP_DAY_ID", tripDay.getId());
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }

            @Override
            public void onError(int status_code) {
                Log.d("TRIP_DAY", status_code + "");
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("TRIP_DAY", throwable + "");
            }
        });
    }
}
