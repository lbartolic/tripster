package com.example.asus.tripplanner.models;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.example.asus.tripplanner.R;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 20.08.16..
 */
public class EventChild {
    @SerializedName("id")
    private int id;
    @SerializedName("confirmation_code")
    private String confirmationCode;
    @SerializedName("host")
    private String host;
    @SerializedName("airline")
    private String airline;
    @SerializedName("transport_number")
    private String transportNumber;
    @SerializedName("terminal")
    private String terminal;
    @SerializedName("gate")
    private String gate;
    @SerializedName("transportation_type")
    private TransportationType transportationType;

    public int getId() {
        return id;
    }

    public String getConfirmationCode() {
        return confirmationCode;
    }

    public String getHost() {
        return host;
    }

    public String getAirline() {
        return airline;
    }

    public String getTransportNumber() {
        return transportNumber;
    }

    public String getTerminal() {
        return terminal;
    }

    public String getGate() {
        return gate;
    }

    public TransportationType getTransportationType() {
        return transportationType;
    }
}
