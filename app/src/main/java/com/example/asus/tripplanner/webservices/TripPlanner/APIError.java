package com.example.asus.tripplanner.webservices.TripPlanner;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by asus on 29.08.16..
 */
public class APIError {

    private int code;
    private String message;

    public APIError() {
    }

    public int code() {
        return code;
    }

    public String message() {
        return message;
    }
}