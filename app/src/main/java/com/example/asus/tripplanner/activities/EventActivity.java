package com.example.asus.tripplanner.activities;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.GooglePlace;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.utils.ValidationUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.EventRepository;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Line;
import com.google.gson.Gson;

import io.techery.properratingbar.ProperRatingBar;

public class EventActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {
    private int tripDayId;
    private int eventId;
    private Event event;
    private Trip trip;
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    private ScrollView svEventDetails;
    private RelativeLayout toolbarInclude;
    private RelativeLayout rlMapHolder;
    private Toolbar toolbar;
    private TextView tvHeading;
    private TextView tvETExtra;
    private TextView tvHeadingExtra;
    private TextView tvEventStartTime;
    private TextView tvTitle;
    private TextView tvFromLabel;
    private TextView tvToLabel;
    private TextView tvTimeFromTime;
    private TextView tvTimeToDate;
    private TextView tvTimeToTime;
    private TextView tvExpenseAmount;
    private TextView tvExpenseCurrency;
    private TextView tvDescription;
    private TextView tvConfirmation;
    private TextView tvFlightNum;
    private TextView tvTerminal;
    private TextView tvGate;
    private TextView tvMapLocSet;
    private TextView tvMapLocName;
    private TextView tvPlaceAddress;
    private TextView tvPlacePhone;
    private TextView tvPlaceWebsite;
    private ProperRatingBar prbPlaceRating;
    private LinearLayout llTimeFrom;
    private LinearLayout llEventTypeExtra;
    private LinearLayout llHeadingExtra;
    private LinearLayout llExtraHolder;
    private LinearLayout llFlightExtraHolder;
    private LinearLayout llMapLocExtra;
    private LinearLayout llMapLoc;
    private ImageView ivHeading;
    private ImageView ivMapLocSet;
    private ImageView ivETExtra;
    private ImageView ivTransparent;
    private MapFragment fragMap;
    private Place place = null;
    private ProgressBar pbEventDetails;
    private ProgressBar pbEvent;
    private SwipeRefreshLayout srlEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Bundle extras = getIntent().getExtras();
        eventId = extras.getInt("EVENT_ID");
        tripDayId = extras.getInt("TRIP_DAY_ID");
        String tripJson = extras.getString("TRIP_JSON");
        trip = new Gson().fromJson(tripJson, Trip.class);
        initUI();
        setEventListeners();
        setEventDetails();
    }

    private void initUI() {
        svEventDetails = (ScrollView) findViewById(R.id.svEventDetails);
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Event Details");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvHeading = (TextView) findViewById(R.id.tvHeading);
        tvETExtra = (TextView) findViewById(R.id.tvETExtra);
        tvHeadingExtra = (TextView) findViewById(R.id.tvHeadingExtra);
        tvEventStartTime = (TextView) findViewById(R.id.tvEventStartTime);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvFromLabel = (TextView) findViewById(R.id.tvFromLabel);
        tvToLabel = (TextView) findViewById(R.id.tvToLabel);
        tvTimeFromTime = (TextView) findViewById(R.id.tvTimeFromTime);
        tvTimeToDate = (TextView) findViewById(R.id.tvTimeToDate);
        tvTimeToTime = (TextView) findViewById(R.id.tvTimeToTime);
        tvExpenseAmount = (TextView) findViewById(R.id.tvExpenseAmount);
        tvExpenseCurrency = (TextView) findViewById(R.id.tvExpenseCurrency);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        tvConfirmation = (TextView) findViewById(R.id.tvConfirmation);
        tvFlightNum = (TextView) findViewById(R.id.tvFlightNum);
        tvTerminal = (TextView) findViewById(R.id.tvTerminal);
        tvGate = (TextView) findViewById(R.id.tvGate);
        tvMapLocSet = (TextView) findViewById(R.id.tvMapLocSet);
        tvMapLocName = (TextView) findViewById(R.id.tvMapLocName);
        tvPlaceAddress = (TextView) findViewById(R.id.tvPlaceAddress);
        tvPlacePhone = (TextView) findViewById(R.id.tvPlacePhone);
        tvPlaceWebsite = (TextView) findViewById(R.id.tvPlaceWebsite);
        prbPlaceRating = (ProperRatingBar) findViewById(R.id.prbPlaceRating);
        llTimeFrom = (LinearLayout) findViewById(R.id.llTimeFrom);
        llEventTypeExtra = (LinearLayout) findViewById(R.id.llEventTypeExtra);
        llHeadingExtra = (LinearLayout) findViewById(R.id.llHeadingExtra);
        llExtraHolder = (LinearLayout) findViewById(R.id.llExtraHolder);
        llFlightExtraHolder = (LinearLayout) findViewById(R.id.llFlightExtraHolder);
        llMapLocExtra = (LinearLayout) findViewById(R.id.llMapLocExtra);
        llMapLoc = (LinearLayout) findViewById(R.id.llMapLoc);
        ivHeading = (ImageView) findViewById(R.id.ivHeading);
        ivMapLocSet = (ImageView) findViewById(R.id.ivMapLocSet);
        ivETExtra = (ImageView) findViewById(R.id.ivETExtra);
        ivTransparent = (ImageView) findViewById(R.id.ivTransparent);
        rlMapHolder = (RelativeLayout) findViewById(R.id.rlMapHolder);
        fragMap = (MapFragment) getFragmentManager().findFragmentById(R.id.fragMap);
        fragMap.getMapAsync(this);
        pbEventDetails = (ProgressBar) findViewById(R.id.pbEventDetails);
        pbEvent = (ProgressBar) findViewById(R.id.pbEvent);
        srlEvent = (SwipeRefreshLayout) findViewById(R.id.srlEvent);
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
    }

    private void setEventListeners() {
        ivTransparent.setOnTouchListener(new View.OnTouchListener() {
            /*
             * Credits: Laksh
             * stackoverflow.com/questions/16974983/
             * google-maps-api-v2-supportmapfragment-inside-scrollview-users-cannot-scroll-th
             *
             * - disallowing ScrollView to intercept touch events
             * (enabling map touch events in ScrollView)
             */
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        svEventDetails.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        svEventDetails.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        svEventDetails.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });

        srlEvent.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setEventDetails();
            }
        });
    }

    private void setEventDetails() {
        /* TODO:
         * Reuse code from EventsListViewAdapter.
         */
        EventRepository.getEvent(User.getAuthUserApiToken(this), trip.getId(), tripDayId, eventId, new EventResponse() {
            @Override
            public void onSuccess(Event event) {
                EventActivity.this.event = event;
                String fromLabel = "From";
                String toLabel = "To";

                switch (event.getEventType().getType()) {
                    case "accommodation":
                        fromLabel = "Check-in";
                        toLabel = "Check-out";
                        if (event.getEventChild().getHost() == null) llHeadingExtra.setVisibility(View.VISIBLE);
                        llExtraHolder.setVisibility(View.VISIBLE);

                        tvHeadingExtra.setText(event.getEventChild().getHost());
                        tvConfirmation.setText(ValidationUtil.getStringOrNotSet(event.getEventChild().getConfirmationCode()));
                        break;
                    case "transportation":
                        fromLabel = "Departure";
                        toLabel = "Arrival";
                        llHeadingExtra.setVisibility(View.VISIBLE);
                        llExtraHolder.setVisibility(View.VISIBLE);
                        llEventTypeExtra.setVisibility(View.VISIBLE);
                        llFlightExtraHolder.setVisibility(View.VISIBLE);

                        ivETExtra.setImageDrawable(event.getEventChild().getTransportationType().getIcon(EventActivity.this));
                        tvETExtra.setText(event.getEventChild().getTransportationType().getTypeName());
                        tvHeadingExtra.setText(event.getEventChild().getAirline());
                        tvConfirmation.setText(ValidationUtil.getStringOrNotSet(event.getEventChild().getConfirmationCode()));
                        tvFlightNum.setText(ValidationUtil.getStringOrNotSet(event.getEventChild().getTransportNumber()));
                        tvTerminal.setText(ValidationUtil.getStringOrNotSet(event.getEventChild().getTerminal()));
                        tvGate.setText(ValidationUtil.getStringOrNotSet(event.getEventChild().getGate()));
                        break;
                }

                String startTimeFormated = null;
                String toDateFormated = null;
                String toTimeFormated = null;
                if (event.getFromTime() != null) {
                    startTimeFormated = DateUtil.formatDateString(event.getFromTime(), DateUtil.DB_TIME_FORMAT, DateUtil.APP_TIME_FORMAT);
                }
                if (event.getToDate() != null) {
                    toDateFormated = DateUtil.formatDateString(event.getToDate(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_SHORT);
                }
                if (event.getToTime() != null) {
                    toTimeFormated = DateUtil.formatDateString(event.getToTime(), DateUtil.DB_TIME_FORMAT, DateUtil.APP_TIME_FORMAT);
                }

                if (startTimeFormated == null) {
                    llTimeFrom.setVisibility(View.GONE);
                }
                tvFromLabel.setText(fromLabel);
                tvToLabel.setText(toLabel);

                tvHeading.setText(event.getEventType().getTypeName());
                ivHeading.setImageDrawable(event.getEventType().getIcon(EventActivity.this));
                tvTitle.setText(event.getTitle());
                tvEventStartTime.setText(startTimeFormated);
                tvTimeFromTime.setText(ValidationUtil.getStringOrNotSet(startTimeFormated));
                tvTimeToDate.setText(ValidationUtil.getStringOrNotSet(toDateFormated));
                tvTimeToTime.setText(ValidationUtil.getStringOrNotSet(toTimeFormated));
                tvExpenseAmount.setText(ValidationUtil.getStringOrNotSet(event.getExpenseAmount()));
                tvExpenseCurrency.setText(trip.getExpenseCurrency());
                tvDescription.setText(ValidationUtil.getStringOrNotSet(event.getDescription()));

                stopSwipeRefresh();
                pbEvent.setVisibility(View.GONE);
                svEventDetails.setVisibility(View.VISIBLE);
                if (googleMap != null) mapReadySetup();
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    public void stopSwipeRefresh() {
        srlEvent.setRefreshing(false);
    }

    private void setMapLocationDetails() {
        tvMapLocSet.setText("Location set");
        ivMapLocSet.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_black_24dp));
        tvMapLocSet.setTextColor(getResources().getColor(R.color.enabled));
        ivMapLocSet.setColorFilter(getResources().getColor(R.color.enabled));
        tvMapLocName.setText(place.getName());

        if (place.getAddress() != null) {
            tvPlaceAddress.setText(place.getAddress());
        }
        if (place.getPhoneNumber() != null) {
            tvPlacePhone.setText(place.getPhoneNumber().toString());
        }
        if (place.getWebsiteUri() != null) {
            tvPlaceWebsite.setText(String.valueOf(place.getWebsiteUri()));
        }
        if (place.getRating() != -1) {
            prbPlaceRating.setRating(Math.round(place.getRating()));
            prbPlaceRating.setVisibility(View.VISIBLE);
        }
        Log.i("RATING", place.getRating() + "");
        Log.i("RATING", Math.round(place.getRating()) + "");

        pbEventDetails.setVisibility(View.GONE);
        tvMapLocName.setVisibility(View.VISIBLE);
        llMapLocExtra.setVisibility(View.VISIBLE);
        rlMapHolder.setVisibility(View.VISIBLE);
        llMapLoc.setVisibility(View.VISIBLE);
    }

    private void setMapLocationNotSet() {
        pbEventDetails.setVisibility(View.GONE);
        tvMapLocSet.setText("Location not set");
        tvMapLocSet.setTextColor(getResources().getColor(R.color.disabled));
        ivMapLocSet.setVisibility(View.GONE);
        llMapLoc.setVisibility(View.VISIBLE);
    }

    private void setMapView() {
        LatLng coords = place.getLatLng();
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(coords.latitude, coords.longitude))
                .title(place.getName().toString()));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(coords.latitude, coords.longitude))
                .zoom(GooglePlace.DEFAULT_ZOOM)                   // zoom
                .bearing(0)                 // orientation
                .tilt(0)                    // tilt
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (event != null) {
            mapReadySetup();
        }
    }

    private void mapReadySetup() {
        if (event.getGooglePlace() != null) {
            Places.GeoDataApi.getPlaceById(googleApiClient, event.getGooglePlace().getPlaceIdentifier())
                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
                        @Override
                        public void onResult(PlaceBuffer places) {
                            if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                place = places.get(0);
                                setMapLocationDetails();
                                setMapView();
                                Log.i("PLACE_LAT_LNG", place.getLatLng().latitude + " " + place.getLatLng().longitude);
                                Log.i("PLACE_FOUND", "Place found: " + place.getName());
                            } else {
                                Log.e("PLACE_FOUND", "Place not found");
                            }
                            places.release();
                        }
                    });
        } else {
            setMapLocationNotSet();
        }
    }


    @Override
    public void onClick(View view) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
