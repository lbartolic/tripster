package com.example.asus.tripplanner.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.activities.HomeActivity;
import com.example.asus.tripplanner.adapters.TripsListViewAdapter;
import com.example.asus.tripplanner.models.Trip;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 15.08.16..
 */
public class PastTripsFragment extends Fragment {
    /* TODO: make same fragment for UpcomingTripsFragment and PastTripsFragment (similar functionality) */
    private Type listType = new TypeToken<ArrayList<Trip>>(){}.getType();
    private List<Trip> tripList;
    private TripsListViewAdapter lvAdapter;
    private ListView lvTrips;
    private SwipeRefreshLayout srlTrips;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upcoming_trips, container, false);

        lvTrips = (ListView) v.findViewById(R.id.lvTrips);

        String tripListJson = getArguments().getString("TRIPS");
        Log.i("TRIPLIST_JSON", tripListJson);
        tripList = new Gson().fromJson(tripListJson, listType);
        Log.i("TRIPLIST", tripList.size() + "");

        srlTrips = (SwipeRefreshLayout) v.findViewById(R.id.srlTrips);

        setEventListeners();
        setTripList();
        return v;
    }

    private void setEventListeners() {
        srlTrips.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((HomeActivity) getActivity()).refreshData(null, PastTripsFragment.this, false);
            }
        });
    }

    private void setTripList() {
        lvAdapter = new TripsListViewAdapter(getActivity(), tripList);
        lvTrips.setAdapter(lvAdapter);
        lvTrips.setFocusable(false);
    }

    public void stopSwipeRefresh() {
        srlTrips.setRefreshing(false);
    }
}
