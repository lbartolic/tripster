package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.EventType;
import com.example.asus.tripplanner.models.Trip;

import java.util.List;

/**
 * Created by asus on 17.08.16..
 */
public interface EventTypeListResponse {
    void onSuccess(List<EventType> eventTypeList);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
