package com.example.asus.tripplanner.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

import com.example.asus.tripplanner.R;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 20.08.16..
 */
public class EventType {
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private String type;
    @SerializedName("type_name")
    private String typeName;
    @SerializedName("color")
    private String color;

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getColor() {
        return color;
    }

    public Drawable getIcon(Context context) {
        Drawable drawable = null;
        switch (this.getType()) {
            case "accommodation":
                drawable = context.getResources().getDrawable(R.drawable.ic_local_hotel_black_24dp);
                break;
            case "attraction":
                drawable = context.getResources().getDrawable(R.drawable.ic_menu_camera);
                break;
            case "activity":
                drawable = context.getResources().getDrawable(R.drawable.ic_more_black_24dp);
                break;
            case "food_and_drink":
                drawable = context.getResources().getDrawable(R.drawable.ic_local_pizza_black_24dp);
                break;
            case "shopping":
                drawable = context.getResources().getDrawable(R.drawable.ic_shopping_basket_black_24dp);
                break;
            case "transportation":
                drawable = context.getResources().getDrawable(R.drawable.ic_directions_bus_black_24dp);
                break;

        }
        return drawable;
    }

    public BitmapDescriptor convertDrawableToBitmapDesc(Context context) {
        BitmapDescriptor bitmapDescriptor = null;
        Drawable drawable = this.getIcon(context);
        drawable.setColorFilter(Color.parseColor(this.getColor()), PorterDuff.Mode.SRC_IN);
        int w = drawable.getIntrinsicWidth();
        int h = drawable.getIntrinsicHeight();
        drawable.setBounds(0, 0, w, h);
        Bitmap bm = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bm);
        canvas.drawColor(Color.parseColor("#CCFFFFFF"));
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bm);
    }
}
