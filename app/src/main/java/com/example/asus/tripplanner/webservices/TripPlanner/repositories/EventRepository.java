package com.example.asus.tripplanner.webservices.TripPlanner.repositories;

import android.util.Log;

import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.webservices.TripPlanner.TripPlannerClient;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripDayResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asus on 17.08.16..
 */
public class EventRepository {
    private static final EventInterface eventInterface = TripPlannerClient.getClient()
            .create(EventInterface.class);

    public EventRepository() {

    }

    public static void getEvent(String api_key, int tripId, int tripDayId, int eventId, final EventResponse eventResponse) {
        Call<Event> call = eventInterface.event(api_key, tripId, tripDayId, eventId);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                if (response.isSuccessful()) {
                    Event event = response.body();
                    //Log.d("TRIP_EVENT", trip.getTripDays().get(0).getEvents().get(0).getChildJson().get("confirmation_code").toString());
                    eventResponse.onSuccess(event);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    eventResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                eventResponse.onFailure(t);
            }
        });
    }

    public static void postCreateOrEdit(String api_key, int tripId, int tripDayId, int eventId, String eventType,
                                        String title, String startTime, String dateTo,
                                  String timeTo, String expense, String description, String confirmationCodeAcc,
                                  String host, String confirmationCodeTra, String transportNumber,
                                  String airline, String terminal, String gate, String transportationType,
                                  String mapPlaceId, String mapPlaceName, String mapPlaceAddress, String mapPlaceLat, String mapPlaceLng,
                                  final EventResponse eventResponse) {
        Call<Event> call = eventInterface.createOrEdit(api_key, tripId, tripDayId, eventId, eventType, title, startTime, dateTo, timeTo, expense,
                description, confirmationCodeAcc, host, confirmationCodeTra, transportNumber, airline,
                terminal, gate, transportationType, mapPlaceId, mapPlaceName, mapPlaceAddress, mapPlaceLat, mapPlaceLng);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                if (response.isSuccessful()) {
                    Event event = response.body();
                    eventResponse.onSuccess(event);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    Log.d("MSG", response.errorBody() + "");
                    Log.d("MSG", response.body() + "");
                    Log.d("MSG", response.code() + "");
                    eventResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                eventResponse.onFailure(t);
            }
        });
    }

    public static void destroyEvent(String api_key, int tripId, int tripDayId, int eventId, final EventResponse eventResponse) {
        Call<Event> call = eventInterface.destroyEvent(api_key, tripId, tripDayId, eventId);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                if (response.isSuccessful()) {
                    Event event = response.body();
                    eventResponse.onSuccess(event);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    Log.d("MSG", response.code() + "");
                    eventResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                eventResponse.onFailure(t);
            }
        });
    }
}