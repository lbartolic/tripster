package com.example.asus.tripplanner.models;

import android.content.Context;

import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.utils.PreferencesUtil;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asus on 13.08.16..
 */
public class Invitation {
    @SerializedName("id")
    private int id;
    @SerializedName("trip")
    private Trip trip;
    @SerializedName("sender")
    private User sender;
    @SerializedName("receiver")
    private User receiver;
    @SerializedName("status")
    private int status;
    @SerializedName("created_at")
    private String createdAt;


    public int getId() {
        return id;
    }

    public Trip getTrip() {
        return trip;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public int getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}
