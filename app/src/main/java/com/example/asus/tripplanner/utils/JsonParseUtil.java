package com.example.asus.tripplanner.utils;

import com.google.gson.Gson;

/**
 * Created by asus on 14.08.16..
 */
public class JsonParseUtil {
    private static Gson gson = new Gson();

    public static String convertObjectToJson(Object object) {
        return gson.toJson(object);
    }
}
