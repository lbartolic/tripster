package com.example.asus.tripplanner.webservices.TripPlanner.repositories;

import android.util.Log;

import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.webservices.TripPlanner.APIError;
import com.example.asus.tripplanner.webservices.TripPlanner.TripPlannerClient;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.InvitationResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserResponse;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by asus on 14.08.16..
 */
public class InvitationRepository {
    private static final InvitationInterface invitationInterface = TripPlannerClient.getClient()
            .create(InvitationInterface.class);

    public InvitationRepository() {

    }

    public static void postCreate(String api_key, int tripId, String email, final InvitationResponse invitationResponse) {
        Call<Invitation> call = invitationInterface.create(api_key, tripId, email);
        call.enqueue(new Callback<Invitation>() {
            @Override
            public void onResponse(Call<Invitation> call, Response<Invitation> response) {
                if (response.isSuccessful()) {
                    Invitation trip = response.body();
                    invitationResponse.onSuccess(trip);
                }
                else {
                    Converter<ResponseBody, APIError> converter
                            = TripPlannerClient.getClient().responseBodyConverter(APIError.class, new Annotation[0]);
                    try {
                        APIError errorResponse = converter.convert(response.errorBody());
                        invitationResponse.onError(response.code(), errorResponse.message());
                    } catch (IOException e) {
                        invitationResponse.onError(response.code(), "");
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<Invitation> call, Throwable t) {
                invitationResponse.onFailure(t);
            }
        });
    }

    public static void postReject(String api_key, int invitationId, final InvitationResponse invitationResponse)
    {
        Call<Invitation> call = invitationInterface.reject(api_key, invitationId);
        call.enqueue(new Callback<Invitation>() {
            @Override
            public void onResponse(Call<Invitation> call, Response<Invitation> response) {
                if (response.isSuccessful()) {
                    Invitation invitation = response.body();
                    invitationResponse.onSuccess(invitation);
                }
                else {
                    invitationResponse.onError(response.code(), "");
                }
            }
            @Override
            public void onFailure(Call<Invitation> call, Throwable t) {
                invitationResponse.onFailure(t);
            }
        });
    }

    public static void postAccept(String api_key, int invitationId, final InvitationResponse invitationResponse)
    {
        Call<Invitation> call = invitationInterface.accept(api_key, invitationId);
        call.enqueue(new Callback<Invitation>() {
            @Override
            public void onResponse(Call<Invitation> call, Response<Invitation> response) {
                if (response.isSuccessful()) {
                    Invitation invitation = response.body();
                    invitationResponse.onSuccess(invitation);
                }
                else {
                    invitationResponse.onError(response.code(), "");
                }
            }
            @Override
            public void onFailure(Call<Invitation> call, Throwable t) {
                invitationResponse.onFailure(t);
            }
        });
    }

    public static void postRemoveSent(String api_key, int invitationId, final InvitationResponse invitationResponse)
    {
        Call<Invitation> call = invitationInterface.removeSent(api_key, invitationId);
        call.enqueue(new Callback<Invitation>() {
            @Override
            public void onResponse(Call<Invitation> call, Response<Invitation> response) {
                if (response.isSuccessful()) {
                    Invitation invitation = response.body();
                    invitationResponse.onSuccess(invitation);
                }
                else {
                    invitationResponse.onError(response.code(), "");
                }
            }
            @Override
            public void onFailure(Call<Invitation> call, Throwable t) {
                invitationResponse.onFailure(t);
            }
        });
    }

    public static void getInvitations(String api_key, final InvitationListResponse invitationListResponse) {
        Call<List<Invitation>> call = invitationInterface.invitations(api_key);
        call.enqueue(new Callback<List<Invitation>>() {
            @Override
            public void onResponse(Call<List<Invitation>> call, Response<List<Invitation>> response) {
                if (response.isSuccessful()) {
                    List<Invitation> invitationList = response.body();
                    invitationListResponse.onSuccess(invitationList);
                }
                else {
                    invitationListResponse.onError(response.code(), "");
                }
            }
            @Override
            public void onFailure(Call<List<Invitation>> call, Throwable t) {
                invitationListResponse.onFailure(t);
            }
        });
    }

    public static void getNewReceived(String api_key, final InvitationListResponse invitationListResponse) {
        Call<List<Invitation>> call = invitationInterface.newReceived(api_key);
        call.enqueue(new Callback<List<Invitation>>() {
            @Override
            public void onResponse(Call<List<Invitation>> call, Response<List<Invitation>> response) {
                if (response.isSuccessful()) {
                    List<Invitation> invitationList = response.body();
                    invitationListResponse.onSuccess(invitationList);
                }
                else {
                    invitationListResponse.onError(response.code(), "");
                }
            }
            @Override
            public void onFailure(Call<List<Invitation>> call, Throwable t) {
                invitationListResponse.onFailure(t);
            }
        });
    }
}
