package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.EventType;
import com.example.asus.tripplanner.models.Invitation;

import java.util.List;

/**
 * Created by asus on 17.08.16..
 */
public interface InvitationResponse {
    void onSuccess(Invitation invitation);
    void onError(int status_code, String message);
    void onFailure(Throwable throwable);
}
