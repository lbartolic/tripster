package com.example.asus.tripplanner.utils;

import android.widget.EditText;

/**
 * Created by asus on 23.08.16..
 */
public class ValidationUtil {
    public static boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    public static boolean isEmpty(String text) {
        return text.trim().length() == 0;
    }

    public static String getStringOrNull(EditText editText) {
        if (editText == null) return null;
        if (ValidationUtil.isEmpty(editText)) return null;
        else return editText.getText().toString();
    }

    public static String getStringOrNull(String text) {
        if (text == null) return null;
        if (ValidationUtil.isEmpty(text)) return null;
        else return text;
    }

    public static String getStringOrNotSet(String text) {
        String value = ValidationUtil.getStringOrNull(text);
        if (value == null) return "\u2014";
        else return value;
    }
}
