package com.example.asus.tripplanner.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.User;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*String currentPrefs = "not set";
        if (sharedPrefs.contains("current_user")) {
            currentPrefs = sharedPrefs.getString("current_user", null);
            Log.i("CURR_USR", currentPrefs);
        }
        else {
            sharedPrefs.edit().putString("current_user", "asd").apply();
            currentPrefs = sharedPrefs.getString("current_user", null);
            Log.i("CURR_USR", currentPrefs);
        }*/
        /*User.setUserPrefs(this);
        Log.i("CURR_USR", sharedPrefs.getString(User.getPrefsKey(), null));*/

        //User.removeUserPrefs(this);
        callNextActivity();
    }

    public void callNextActivity() {
        Intent intent;
        if (User.isLoggedIn(this)) {
            Log.d("LOGGED_IN", "true");
            intent = new Intent(this, HomeActivity.class);
        }
        else {
            Log.d("LOGGED_IN", "false");
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
