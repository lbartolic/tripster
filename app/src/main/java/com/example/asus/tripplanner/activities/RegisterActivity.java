package com.example.asus.tripplanner.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.UserRepository;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {
    @NotEmpty
    @Password
    private EditText etPassword;
    @NotEmpty
    private EditText etFirstName;
    @NotEmpty
    @Email
    private EditText etEmail;
    @NotEmpty
    private EditText etLastName;
    @NotEmpty
    @ConfirmPassword
    private EditText etPasswordRepeat;
    private Button btnRegister;
    private ProgressDialog progressDialog;
    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        validator = new Validator(this);

        initUI();
        initListeners();
    }

    private void initUI() {
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPasswordRepeat = (EditText) findViewById(R.id.etPasswordRepeat);
        btnRegister = (Button) findViewById(R.id.btnRegister);
    }

    private void initListeners() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });
        validator.setValidationListener(this);
    }

    private void attempUserRegister() {
        progressDialog = ProgressDialog.show(this, null, "Signing up", true);
        UserRepository.postRegister(etEmail.getText().toString(), etPassword.getText().toString(),
                etPasswordRepeat.getText().toString(), etFirstName.getText().toString(),
                etLastName.getText().toString(), new UserResponse() {
                    @Override
                    public void onSuccess(User user) {
                        progressDialog.hide();
                        User.setUserPrefs(RegisterActivity.this, user);
                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(int status_code) {
                        progressDialog.hide();
                        if (status_code == 403) {
                            showMessage("Sorry, but that email address is already in use.");
                        }
                        showMessage("Oops! Something's wrong. Please try again.");
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        progressDialog.hide();
                    }
                });
    }


    private void showMessage(String message) {
        final Dialog messageDialog = new Dialog(this);
        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(R.layout.dialog_message);
        Button btnClose = (Button) messageDialog.findViewById(R.id.btnClose);
        final TextView tvText = (TextView) messageDialog.findViewById(R.id.tvText);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageDialog.dismiss();
            }
        });
        tvText.setText(message);
        messageDialog.show();
    }

    private void handleSuccessfulAuth(User user) {
        User.setUserPrefs(this, user);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onValidationSucceeded() {
        attempUserRegister();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
