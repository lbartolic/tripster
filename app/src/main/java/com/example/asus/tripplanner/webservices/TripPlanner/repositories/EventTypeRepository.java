package com.example.asus.tripplanner.webservices.TripPlanner.repositories;

import android.util.Log;

import com.example.asus.tripplanner.models.EventType;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.webservices.TripPlanner.TripPlannerClient;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventTypeInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventTypeListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asus on 17.08.16..
 */
public class EventTypeRepository {
    private static final EventTypeInterface eventTypeInterface = TripPlannerClient.getClient()
            .create(EventTypeInterface.class);

    public EventTypeRepository() {

    }

    public static void getEventTypes(String api_key, final EventTypeListResponse eventTypeListResponse) {
        Call<List<EventType>> call = eventTypeInterface.eventTypes(api_key);
        call.enqueue(new Callback<List<EventType>>() {
            @Override
            public void onResponse(Call<List<EventType>> call, Response<List<EventType>> response) {
                if (response.isSuccessful()) {
                    List<EventType> eventTypeList = response.body();
                    eventTypeListResponse.onSuccess(eventTypeList);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    Log.d("MSG", response.message());
                    eventTypeListResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<List<EventType>> call, Throwable t) {
                eventTypeListResponse.onFailure(t);
            }
        });
    }
}
