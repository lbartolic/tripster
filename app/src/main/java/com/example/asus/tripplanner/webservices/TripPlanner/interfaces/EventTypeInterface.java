package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.EventType;
import com.example.asus.tripplanner.models.Trip;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by asus on 17.08.16..
 */
public interface EventTypeInterface {
    @GET("{api_key}/eventTypes")
    Call<List<EventType>> eventTypes(@Path("api_key") String apiKey);
}
