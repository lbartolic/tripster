package com.example.asus.tripplanner.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asus on 16.08.16..
 */
public class Trip {
    private static final String PREFS_KEY = "trips";

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("expense_currency")
    private String expenseCurrency;
    @SerializedName("description")
    private String description;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("trip_days")
    private List<TripDay> tripDays;
    @SerializedName("events_count")
    private int eventsCount;
    @SerializedName("user")
    private User user;
    @SerializedName("users")
    private List<User> users;
    @SerializedName("is_past")
    boolean isPast;

    public Trip(String title, String createdAt, String updatedAt) {
        this.title = title;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public static String getPrefsKey() {
        return PREFS_KEY;
    }

    public String getTitle() {
        return title;
    }

    public String getExpenseCurrency() {
        return expenseCurrency;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getStartDate() {
        return startDate;
    }

    public List<TripDay> getTripDays() {
        return tripDays;
    }

    public int getEventsCount() {
        return eventsCount;
    }

    public User getUser() {
        return user;
    }

    public List<User> getUsers() {
        return users;
    }

    public boolean isPast() {
        return isPast;
    }
}
