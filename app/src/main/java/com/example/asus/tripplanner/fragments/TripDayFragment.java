package com.example.asus.tripplanner.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.activities.EditDayActivity;
import com.example.asus.tripplanner.activities.EventActivity;
import com.example.asus.tripplanner.activities.MainActivity;
import com.example.asus.tripplanner.activities.NewDayActivity;
import com.example.asus.tripplanner.activities.NewEventActivity;
import com.example.asus.tripplanner.activities.TripActivity;
import com.example.asus.tripplanner.activities.TripPlanActivity;
import com.example.asus.tripplanner.adapters.DayPagerAdapter;
import com.example.asus.tripplanner.adapters.EventsListViewAdapter;
import com.example.asus.tripplanner.adapters.TripsListViewAdapter;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.utils.ListUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.TripPlannerClient;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripDayResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.EventRepository;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by asus on 15.08.16..
 */
public class TripDayFragment extends Fragment {
    private List<Trip> tripList;
    private EventsListViewAdapter lvAdapter;
    private ListView lvEvents;
    private SwipeRefreshLayout srlTripPlan;
    private Trip trip;
    private TripDay tripDay;
    private int tripDayPos;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trip_day, container, false);

        String tripDayJson = getArguments().getString("TRIP_DAY");
        tripDay = new Gson().fromJson(tripDayJson, TripDay.class);
        tripDayPos = getArguments().getInt("TRIP_DAY_POS");
        String tripJson = getArguments().getString("TRIP");
        trip = new Gson().fromJson(tripJson, Trip.class); // from PagerAdapter, used in bundles for other activities as Trip details
        /*String nestedPolymorficRelationTest = trip.getTripDays().get(0).getEvents().get(0).getEventChild().getTransportationType().getTypeName();
        Log.d("NESTED_POLYMORFIC", nestedPolymorficRelationTest); // OUTPUT: Flight*/
        initUI(v);
        setEventListeners();
        setEventList();
        return v;
    }

    private void initUI(View v) {
        lvEvents = (ListView) v.findViewById(R.id.lvEvents);
        srlTripPlan = (SwipeRefreshLayout) v.findViewById(R.id.srlTripPlan);
    }

    private void setEventListeners() {
        srlTripPlan.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((TripPlanActivity) getActivity()).refreshData(trip, TripDayFragment.this, false);
            }
        });
        lvEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_event_item_touch);

                (dialog.findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                (dialog.findViewById(R.id.btnShowMore)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), EventActivity.class);
                        intent.putExtra("TRIP_DAY_ID", trip.getTripDays().get(tripDayPos).getId());
                        intent.putExtra("EVENT_ID", trip.getTripDays().get(tripDayPos).getEvents().get(i - 1).getId());
                        intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                        startActivity(intent);
                    }
                });
                (dialog.findViewById(R.id.btnEdit)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent(getActivity(), NewEventActivity.class);
                        intent.putExtra("EVENT_TYPE_JSON", JsonParseUtil.convertObjectToJson(
                                trip.getTripDays().get(tripDayPos).getEvents().get(i - 1).getEventType()));
                        intent.putExtra("EVENT_EDIT_JSON", JsonParseUtil.convertObjectToJson(
                                trip.getTripDays().get(tripDayPos).getEvents().get(i - 1)));
                        intent.putExtra("TRIP_DAY_POS", tripDayPos);
                        intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                        startActivityForResult(intent, 2);
                    }
                });
                (dialog.findViewById(R.id.btnDelete)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        deleteEvent(trip.getTripDays().get(tripDayPos).getEvents().get(i - 1), i);
                    }
                });
                dialog.show();
            }
        });
        lvEvents.setOnScrollListener(new AbsListView.OnScrollListener() {
            FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fabNewEvent);
            int prevVisibleItem = 0;
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int i1, int i2) {
                if (prevVisibleItem != firstVisibleItem) {
                    if (prevVisibleItem < firstVisibleItem) {
                        ((FloatingActionButton) getActivity().findViewById(R.id.fabNewEvent)).hide();
                    } else {
                        ((FloatingActionButton) getActivity().findViewById(R.id.fabNewEvent)).show();
                    }
                }
                prevVisibleItem = firstVisibleItem;
            }
        });
    }

    private void deleteEvent(final Event event, final int position) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        ((TextView) dialog.findViewById(R.id.tvConfirmText)).setText("Delete event " + event.getTitle());
        (dialog.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventRepository.destroyEvent(User.getAuthUserApiToken(getActivity()), trip.getId(),
                        tripDay.getId(), event.getId(), new EventResponse() {
                            @Override
                            public void onSuccess(Event event) {
                                dialog.hide();
                                ((TripPlanActivity) getActivity()).refreshData(trip, null, true);
                            }

                            @Override
                            public void onError(int status_code) {
                                dialog.hide();
                            }

                            @Override
                            public void onFailure(Throwable throwable) {
                                dialog.hide();
                            }
                        });
            }
        });
        dialog.show();
    }

    public void stopSwipeRefresh() {
        srlTripPlan.setRefreshing(false);
    }

    private void setEventList() {
        lvAdapter = new EventsListViewAdapter(getActivity(), tripDay, trip);
        lvEvents.setAdapter(lvAdapter);
        LayoutInflater inflater = getLayoutInflater(null);
        View header = inflater.inflate(R.layout.header_trip_day_list, lvEvents, false);
        TextView tvDayTitle = (TextView) header.findViewById(R.id.tvDayTitle);
        TextView tvDayDate = (TextView) header.findViewById(R.id.tvDayDate);
        String dayDateString = DateUtil.convertDateToString(tripDay.getCalculatedDate(trip), DateUtil.APP_DATE_FORMAT_LONG);
        tvDayTitle.setText(tripDay.getTitle());
        tvDayDate.setText(dayDateString);
        final ImageView ivEditTripDay = (ImageView) header.findViewById(R.id.ivEditTripDay);
        ivEditTripDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(getActivity(), ivEditTripDay);
                popup.getMenuInflater()
                        .inflate(R.menu.popup_menu_trip_day, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.edit:
                                editTripDay(tripDay);
                                break;
                            case R.id.delete:
                                destroyTripDay(tripDay);
                                break;
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

        lvEvents.addHeaderView(header, null, false);
    }

    private void destroyTripDay(final TripDay tripDay) {
        Log.d("TD", tripDay.getId() + "");
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        ((TextView) dialog.findViewById(R.id.tvConfirmText)).setText("Delete trip day " + tripDay.getTitle());
        (dialog.findViewById(R.id.btnNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        (dialog.findViewById(R.id.btnYes)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TripRepository.deleteDestroyTripDay(User.getAuthUserApiToken(getActivity()), tripDay.getId(), new TripDayResponse() {
                    @Override
                    public void onSuccess(TripDay tripDay) {
                        dialog.dismiss();
                        ((TripPlanActivity) getActivity()).refreshData(trip, null, true);
                    }

                    @Override
                    public void onError(int status_code) {

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });
        dialog.show();
    }

    private void editTripDay(TripDay tripDay) {
        Intent intent = new Intent(getActivity(), EditDayActivity.class);
        intent.putExtra("TRIP_DAY_JSON", JsonParseUtil.convertObjectToJson(tripDay));
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                int result = data.getIntExtra("TRIP_DAY_ID", -1);
                ((TripPlanActivity) getActivity()).refreshData(trip, null, true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){
                ((TripPlanActivity) getActivity()).refreshData(trip, null, true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}
