package com.example.asus.tripplanner.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.adapters.ExpensesListViewAdapter;
import com.example.asus.tripplanner.adapters.TripsListViewAdapter;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.utils.ListUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.EventListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.List;

public class ExpensesActivity extends AppCompatActivity {
    private List<Event> eventList;
    private Trip trip;
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private ProgressBar pbExpenses;
    private SwipeRefreshLayout srlExpenses;
    private ListView lvExpenses;
    private TextView tvTripTitle;
    private TextView tvTtlExpenseAmount;
    private TextView tvTtlExpenseCurrency;
    private ExpensesListViewAdapter lvAdapter;
    private LinearLayout llTotalHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);

        Bundle extras = getIntent().getExtras();
        String tripJson = extras.getString("TRIP_JSON");
        trip = new Gson().fromJson(tripJson, Trip.class);
        initUI();
        setEventListeners();

        setExpensesList();
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Trip Expenses");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        pbExpenses = (ProgressBar) findViewById(R.id.pbExpenses);
        srlExpenses = (SwipeRefreshLayout) findViewById(R.id.srlExpenses);
        lvExpenses = (ListView) findViewById(R.id.lvExpenses);
        tvTripTitle = (TextView) findViewById(R.id.tvTripTitle);
        tvTtlExpenseAmount = (TextView) findViewById(R.id.tvTtlExpenseAmount);
        tvTtlExpenseCurrency = (TextView) findViewById(R.id.tvTtlExpenseCurrency);
        llTotalHolder = (LinearLayout) findViewById(R.id.llTotalHolder);

        tvTripTitle.setText(trip.getTitle());
    }

    private void setEventListeners() {
        srlExpenses.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setExpensesList();
            }
        });
    }

    private void setExpensesList() {
        TripRepository.getTripExpenses(User.getAuthUserApiToken(this), trip.getId(), new EventListResponse() {
            @Override
            public void onSuccess(final List<Event> eventList) {
                ExpensesActivity.this.eventList = eventList;
                lvAdapter = new ExpensesListViewAdapter(ExpensesActivity.this, eventList, trip);
                lvExpenses.setAdapter(lvAdapter);
                lvExpenses.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(ExpensesActivity.this, EventActivity.class);
                        intent.putExtra("TRIP_DAY_ID", eventList.get(i).getTripDayId());
                        intent.putExtra("EVENT_ID", eventList.get(i).getId());
                        intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                        startActivity(intent);
                    }
                });
                setTotalExpense();
                stopSwipeRefresh();
                pbExpenses.setVisibility(View.GONE);
                lvExpenses.setVisibility(View.VISIBLE);
                llTotalHolder.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    public void stopSwipeRefresh() {
        srlExpenses.setRefreshing(false);
    }

    private void setTotalExpense() {
        double totalAmount = 0.00;
        for (int i = 0; i < eventList.size(); i++) {
            totalAmount += Double.parseDouble(eventList.get(i).getExpenseAmount());
        }
        tvTtlExpenseAmount.setText(String.format("%.2f", totalAmount));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
