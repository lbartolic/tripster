package com.example.asus.tripplanner.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.GooglePlace;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.DateUtil;
import com.example.asus.tripplanner.utils.JsonParseUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.PlaceListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.github.aakira.expandablelayout.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MapLocationsActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener {
    private Trip trip;
    private List<GooglePlace> googlePlaces; // all places (constant)
    private List<GooglePlace> gPlaces = null; // filtered places by day (dynamic)
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;
    private MapFragment fragMap;
    private RelativeLayout toolbarInclude;
    private Toolbar toolbar;
    private LinearLayout llDaysHolder;
    private LinearLayout llEventDetails;
    private LinearLayout llDayInfoHolder;
    private TextView tvDayDate;
    private TextView tvEventsCount;
    private TabLayout tlDay;
    private Button btnEventDetails;
    private boolean menuZoomOutVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_locations);

        Bundle extras = getIntent().getExtras();
        String tripJson = extras.getString("TRIP_JSON");
        trip = new Gson().fromJson(tripJson, Trip.class);

        initUI();
        setEventListeners();
    }

    private void initUI() {
        toolbarInclude = (RelativeLayout) findViewById(R.id.toolbarInclude);
        toolbar = (Toolbar) toolbarInclude.findViewById(R.id.toolbar);
        toolbar.setTitle("Map Locations");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fragMap = (MapFragment) getFragmentManager().findFragmentById(R.id.fragMap);
        fragMap.getMapAsync(this);
        llDaysHolder = (LinearLayout) findViewById(R.id.llDaysHolder);
        tlDay = (TabLayout) findViewById(R.id.tlTop);
        llEventDetails = (LinearLayout) findViewById(R.id.llEventDetails);
        llDayInfoHolder = (LinearLayout) findViewById(R.id.llDayInfoHolder);
        tvDayDate = (TextView) findViewById(R.id.tvDayDate);
        tvEventsCount = (TextView) findViewById(R.id.tvEventsCount);
        btnEventDetails = (Button) findViewById(R.id.btnEventDetails);
    }

    private void setEventListeners() {

    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        TripRepository.getTripPlaces(User.getAuthUserApiToken(this), trip.getId(), new PlaceListResponse() {
            @Override
            public void onSuccess(List<GooglePlace> googlePlaces) {
                MapLocationsActivity.this.googlePlaces = googlePlaces;
                gPlaces = null;
                setMapView();
                setTabs();
                setTabDayInfo(0);
            }

            @Override
            public void onError(int status_code) {

            }

            @Override
            public void onFailure(Throwable throwable) {

            }
        });
    }

    private void setTabDayInfo(int position) {
        int index = position;
        int eventsCount = 0;
        if (gPlaces != null) eventsCount = gPlaces.size();
        if (index == 0) {
            String fromDate = DateUtil.convertDateToString(trip.getTripDays().get(0).getCalculatedDate(trip),
                    DateUtil.APP_DATE_FORMAT_LONG);
            String toDate = DateUtil.convertDateToString(trip.getTripDays().get(trip.getTripDays().size() - 1).getCalculatedDate(trip),
                    DateUtil.APP_DATE_FORMAT_LONG);
            tvDayDate.setText(fromDate + " — " + toDate);
            eventsCount = googlePlaces.size();
        }
        else {
            tvDayDate.setText(DateUtil.convertDateToString(
                    trip.getTripDays().get(index - 1).getCalculatedDate(trip), DateUtil.APP_DATE_FORMAT_LONG));
        }
        tvEventsCount.setText("EVENTS: " + String.valueOf(eventsCount));
    }

    private void setTabs() {
        String txt;
        for (int i = -1; i < trip.getTripDays().size(); i++) {
            txt = (i == -1) ? "ALL DAYS" : "DAY " + (i + 1);
            tlDay.addTab(tlDay.newTab().setText(txt));
        }
        tlDay.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.i("TAB", tab.getPosition() + "");
                tabSelected(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabSelected(tab);
            }
        });
        tlDay.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void tabSelected(TabLayout.Tab tab) {
        menuZoomOutVisible = false;
        invalidateOptionsMenu();
        llEventDetails.setVisibility(View.GONE);
        if (tab.getPosition() > 0) {
            int tripDayIndex = tab.getPosition() - 1;
            gPlaces = getPlacesByTripDay(trip.getTripDays().get(tripDayIndex).getId());
            setMapView();
        }
        else {
            gPlaces = null;
            setMapView();
        }
        setTabDayInfo(tab.getPosition());
    }

    private List<GooglePlace> getPlacesByTripDay(int tripDayId) {
        List<GooglePlace> gPlaces = new ArrayList<>();
        for (int i = 0; i < googlePlaces.size() ; i++) {
            Log.i("PLACE_TRIP_DAY_ID", googlePlaces.get(i).getEvent().getTripDay().getId() + "");
            Log.i("TRIP_DAY_ID", tripDayId + "");
            if (googlePlaces.get(i).getEvent().getTripDay().getId() == tripDayId) {
                gPlaces.add(googlePlaces.get(i));
                Log.i("G_PLACES_ADD", googlePlaces.get(i).getId() + "");
            }
        }
        return gPlaces;
    }

    private void setMapView() {
        googleMap.clear();
        List<GooglePlace> googlePlaces = this.googlePlaces;
        if (gPlaces != null) {
            googlePlaces = gPlaces;
        }
        if (googlePlaces != null && googlePlaces.size() > 0) {
            List<Marker> markers = new ArrayList<Marker>();
            for (int i = 0; i < googlePlaces.size(); i++) {
                Log.i("EV", googlePlaces.get(i).getEvent().getTitle());
                Double lat = googlePlaces.get(i).getPlaceLat();
                Double lng = googlePlaces.get(i).getPlaceLng();

                Marker marker = googleMap.addMarker(new MarkerOptions()
                        .icon(googlePlaces.get(i).getEvent().getEventType().convertDrawableToBitmapDesc(this))
                        .position(new LatLng(lat, lng))
                        .title(googlePlaces.get(i).getPlaceName()));
                markers.add(marker);

                marker.setTag(googlePlaces.get(i));
            }
            markers.size();

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            bounds = adjustBoundsForMaxZoomLevel(bounds);
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 150);
            googleMap.animateCamera(cu);

            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    final GooglePlace gPlace = (GooglePlace) marker.getTag();
                    Log.i("GP", gPlace.getPlaceName());
                    CameraPosition cameraPosition = changeCameraPosition(gPlace.getPlaceLat(), gPlace.getPlaceLng(), GooglePlace.DEFAULT_ZOOM);
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    marker.showInfoWindow();

                    llEventDetails.setVisibility(View.VISIBLE);
                    menuZoomOutVisible = true;
                    invalidateOptionsMenu();
                    btnEventDetails.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Event event = gPlace.getEvent();
                            Intent intent = new Intent(MapLocationsActivity.this, EventActivity.class);
                            intent.putExtra("TRIP_DAY_ID", event.getTripDay().getId());
                            intent.putExtra("EVENT_ID", event.getId());
                            intent.putExtra("TRIP_JSON", JsonParseUtil.convertObjectToJson(trip));
                            startActivity(intent);
                        }
                    });
                    return true;
                }
            });
        }
    }

    private CameraPosition changeCameraPosition(Double lat, Double lng, float zoom) {
        return new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(zoom)
                .build();
    }

    /**
     * by kasimir (StackOverflow)
     * http://stackoverflow.com/questions/15700808/setting-max-zoom-level-in-google-maps-android-api-v2
     */
    private LatLngBounds adjustBoundsForMaxZoomLevel(LatLngBounds bounds) {
        LatLng sw = bounds.southwest;
        LatLng ne = bounds.northeast;
        double deltaLat = Math.abs(sw.latitude - ne.latitude);
        double deltaLon = Math.abs(sw.longitude - ne.longitude);

        final double zoomN = 0.05;
        if (deltaLat < zoomN) {
            sw = new LatLng(sw.latitude - (zoomN - deltaLat / 2), sw.longitude);
            ne = new LatLng(ne.latitude + (zoomN - deltaLat / 2), ne.longitude);
            bounds = new LatLngBounds(sw, ne);
        }
        else if (deltaLon < zoomN) {
            sw = new LatLng(sw.latitude, sw.longitude - (zoomN - deltaLon / 2));
            ne = new LatLng(ne.latitude, ne.longitude + (zoomN - deltaLon / 2));
            bounds = new LatLngBounds(sw, ne);
        }

        return bounds;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_map_location, menu);
        menu.getItem(0).setVisible(menuZoomOutVisible);
        return true;
    }

    /**
     * Handle toolbar menu item clicks.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.zoomOut:
                menuZoomOutVisible = false;
                invalidateOptionsMenu();
                setMapView();
                llEventDetails.setVisibility(View.GONE);
                break;
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
