package com.example.asus.tripplanner.webservices.TripPlanner.repositories;

import android.util.Log;

import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.webservices.TripPlanner.TripPlannerClient;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserInterface;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.UserResponse;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by asus on 14.08.16..
 */
public class UserRepository {
    private static final UserInterface userInterface = TripPlannerClient.getClient()
            .create(UserInterface.class);

    public UserRepository() {

    }

    public static void authenticateUser(String[] credentials, final UserResponse userResponse) {
        Call<User> call = userInterface.postAuth(credentials[0], credentials[1]);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    userResponse.onSuccess(user);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    userResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userResponse.onFailure(t);
            }
        });
    }

    public static void postRegister(String email, String password, String passwordRepeat, String firstName,
                                    String lastName, final UserResponse userResponse) {
        Call<User> call = userInterface.postRegister(email, password, passwordRepeat, firstName, lastName);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    User user = response.body();
                    userResponse.onSuccess(user);
                }
                else {
                    /* TO DO... parse error, send status_code/message... */
                    userResponse.onError(response.code());
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userResponse.onFailure(t);
            }
        });
    }
}
