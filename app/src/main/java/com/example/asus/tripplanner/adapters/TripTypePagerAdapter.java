package com.example.asus.tripplanner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.fragments.PastTripsFragment;
import com.example.asus.tripplanner.fragments.UpcomingTripsFragment;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.utils.JsonParseUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 15.08.16..
 */
public class TripTypePagerAdapter extends FragmentStatePagerAdapter {
    private List<Trip> tripList;
    private int[] imageResIds = {
            R.drawable.ic_flight_takeoff_black_24dp,
            R.drawable.ic_flight_land_black_24dp
    };
    private String tabTitles[] = new String[] {
            "UPCOMING", "PAST"
    };
    private Context context;


    public TripTypePagerAdapter(FragmentManager fm, Context ctx, List<Trip> tripList) {
        super(fm);
        this.context = ctx;
        this.tripList = tripList;
    }

    @Override
    public Fragment getItem(int position) {
        List<Trip> utfList = new ArrayList<>();
        List<Trip> ptfList = new ArrayList<>();
        for (int i = 0; i < this.tripList.size() ; i++) {
            if (this.tripList.get(i).isPast()) ptfList.add(this.tripList.get(i));
            else utfList.add(this.tripList.get(i));
        }

        Bundle bundle = new Bundle();
        switch(position) {
            case 0:
                UpcomingTripsFragment utf = new UpcomingTripsFragment();
                bundle.putString("TRIPS", JsonParseUtil.convertObjectToJson(utfList));
                utf.setArguments(bundle);
                return utf;
            case 1:
                PastTripsFragment ptf = new PastTripsFragment();
                bundle.putString("TRIPS", JsonParseUtil.convertObjectToJson(ptfList));
                ptf.setArguments(bundle);
                return ptf;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable image = ContextCompat.getDrawable(context, imageResIds[position]);
        PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Color.WHITE,
                PorterDuff.Mode.SRC_ATOP);
        image.setColorFilter(porterDuffColorFilter);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString("    " + tabTitles[position]);
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
