package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.GooglePlace;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.models.User;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by asus on 17.08.16..
 */
public interface TripInterface {
    @FormUrlEncoded
    @POST("{api_key}/trips")
    Call<Trip> createOrUpdate(@Path("api_key") String api_key, @Field("tripId") int tripId, @Field("title") String title, @Field("description") String description, @Field("start_date") String startDate, @Field("currency") String currency);

    @FormUrlEncoded
    @POST("{api_key}/trips/{trip}/tripDays")
    Call<TripDay> createTripDay(@Path("api_key") String api_key, @Path("trip") int tripId, @Field("title") String title);

    @DELETE("{api_key}/trips/{trip}/leaveTrip")
    Call<Trip> leaveTrip(@Path("api_key") String api_key, @Path("trip") int tripId);

    @FormUrlEncoded
    @PUT("{api_key}/trips/tripDays/{id}")
    Call<TripDay> updateTripDay(@Path("api_key") String api_key, @Path("id") int tripDayId, @Field("title") String title);

    @DELETE("{api_key}/trips/tripDays/{id}")
    Call<TripDay> destroyTripDay(@Path("api_key") String api_key, @Path("id") int tripDayId);

    @GET("{api_key}/trips")
    Call<List<Trip>> trips(@Path("api_key") String apiKey);

    @GET("{api_key}/trips/{id}")
    Call<Trip> trip(@Path("api_key") String apiKey, @Path("id") int id);

    @GET("{api_key}/trips/{id}/places")
    Call<List<GooglePlace>> tripPlaces(@Path("api_key") String apiKey, @Path("id") int id);

    @GET("{api_key}/trips/{id}/expenses")
    Call<List<Event>> tripExpenses(@Path("api_key") String apiKey, @Path("id") int id);
}
