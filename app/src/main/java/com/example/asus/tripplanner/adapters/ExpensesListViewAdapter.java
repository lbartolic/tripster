package com.example.asus.tripplanner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.Trip;

import java.util.List;

/**
 * Created by asus on 16.08.16..
 */
public class ExpensesListViewAdapter extends BaseAdapter {
    private Context context;
    private List<Event> eventList;
    private Trip trip;

    public ExpensesListViewAdapter(Context context, List<Event> eventList, Trip trip) {
        super();
        this.context = context;
        this.eventList = eventList;
        this.trip = trip;
    }

    @Override
    public int getCount() {
        return this.eventList.size();
    }

    @Override
    public Object getItem(int i) {
        return eventList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.list_item_expense, null);
        }
        ImageView ivEventIcon = (ImageView) view.findViewById(R.id.ivEventIcon);
        TextView tvEventType = (TextView) view.findViewById(R.id.tvEventType);
        TextView tvEventTitle = (TextView) view.findViewById(R.id.tvEventTitle);
        TextView tvExpenseAmount = (TextView) view.findViewById(R.id.tvExpenseAmount);
        TextView tvExpenseCurrency = (TextView) view.findViewById(R.id.tvExpenseCurrency);


        ivEventIcon.setImageDrawable(eventList.get(i).getChildTypeIcon(context));
        ivEventIcon.setColorFilter((Color.parseColor(eventList.get(i).getEventType().getColor())));
        tvEventType.setText(eventList.get(i).getEventType().getTypeName());
        tvEventType.setTextColor(Color.parseColor(eventList.get(i).getEventType().getColor()));
        tvEventTitle.setText(eventList.get(i).getTitle());
        tvExpenseAmount.setText(eventList.get(i).getExpenseAmount());
        tvExpenseCurrency.setText(trip.getExpenseCurrency());

        return view;
    }
}
