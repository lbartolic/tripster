package com.example.asus.tripplanner.webservices.TripPlanner.interfaces;

import com.example.asus.tripplanner.models.GooglePlace;
import com.example.asus.tripplanner.models.TripDay;

/**
 * Created by asus on 17.08.16..
 */
public interface PlaceResponse {
    void onSuccess(GooglePlace googlePlace);
    void onError(int status_code);
    void onFailure(Throwable throwable);
}
