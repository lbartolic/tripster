package com.example.asus.tripplanner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.models.Event;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.TripDay;
import com.example.asus.tripplanner.utils.DateUtil;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by asus on 16.08.16..
 */
public class EventsListViewAdapter extends BaseAdapter {
    private Context context;
    private List<Event> eventList;
    private TripDay tripDay;
    private Trip trip;

    public EventsListViewAdapter(Context ctx, TripDay tripDay, Trip trip) {
        super();
        this.context = ctx;
        this.tripDay = tripDay;
        this.eventList = tripDay.getEvents();
        this.trip = trip;
    }

    @Override
    public int getCount() {
        return this.eventList.size();
    }

    @Override
    public int getViewTypeCount() {
        return (getCount() == 0) ? 1 : getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Object getItem(int i) {
        return eventList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = View.inflate(context, R.layout.list_item_event, null);
        }

        LinearLayout llTimeFrom = (LinearLayout) view.findViewById(R.id.llTimeFrom);
        LinearLayout llDetails = (LinearLayout) view.findViewById(R.id.llDetails);
        LinearLayout llTitleExtra = (LinearLayout) view.findViewById(R.id.llTitleExtra);
        TextView tvTitleExtraKey = (TextView) view.findViewById(R.id.tvTitleExtraKey);
        TextView tvTitleExtraValue = (TextView) view.findViewById(R.id.tvTitleExtraValue);
        TextView tvTitleExtraValueBtm = (TextView) view.findViewById(R.id.tvTitleExtraValueBtm);
        TextView tvEventType = (TextView) view.findViewById(R.id.tvEventType);
        TextView tvEventStartTime = (TextView) view.findViewById(R.id.tvEventStartTime);
        TextView tvEventTitle = (TextView) view.findViewById(R.id.tvEventTitle);
        TextView tvFromLabel = (TextView) view.findViewById(R.id.tvFromLabel);
        TextView tvToLabel = (TextView) view.findViewById(R.id.tvToLabel);
        TextView tvTimeFromTime = (TextView) view.findViewById(R.id.tvTimeFromTime);
        TextView tvTimeToDate = (TextView) view.findViewById(R.id.tvTimeToDate);
        TextView tvTimeToTime = (TextView) view.findViewById(R.id.tvTimeToTime);
        TextView tvExpenseAmount = (TextView) view.findViewById(R.id.tvExpenseAmount);
        TextView tvExpenseCurrency = (TextView) view.findViewById(R.id.tvExpenseCurrency);
        ImageView ivEventIcon = (ImageView) view.findViewById(R.id.ivEventIcon);
        LinearLayout llEventTypeExtra = (LinearLayout) view.findViewById(R.id.llEventTypeExtra);
        ImageView ivETExtra = (ImageView) view.findViewById(R.id.ivETExtra);
        TextView tvETExtra = (TextView) view.findViewById(R.id.tvETExtra);

        String fromLabel = "From";
        String toLabel = "To";

        switch (eventList.get(i).getEventType().getType()) {
            case "accommodation":
                fromLabel = "Check-in";
                toLabel = "Check-out";
                break;
            case "transportation":
                fromLabel = "Departure";
                toLabel = "Arrival";
                llEventTypeExtra.setVisibility(View.VISIBLE);
                ivETExtra.setImageDrawable(eventList.get(i).getEventChild().getTransportationType().getIcon(context));
                tvETExtra.setText(eventList.get(i).getEventChild().getTransportationType().getTypeName());
                break;
        }

        String startTimeFormated = null;
        String toDateFormated = null;
        String toTimeFormated = null;
        if (eventList.get(i).getFromTime() != null) {
            startTimeFormated = DateUtil.formatDateString(eventList.get(i).getFromTime(), DateUtil.DB_TIME_FORMAT, DateUtil.APP_TIME_FORMAT);
        }
        if (eventList.get(i).getToDate() != null) {
            toDateFormated = DateUtil.formatDateString(eventList.get(i).getToDate(), DateUtil.DB_DATE_FORMAT, DateUtil.APP_DATE_FORMAT_SHORT);
        }
        if (eventList.get(i).getToTime() != null) {
            toTimeFormated = DateUtil.formatDateString(eventList.get(i).getToTime(), DateUtil.DB_TIME_FORMAT, DateUtil.APP_TIME_FORMAT);
        }

        tvFromLabel.setText(fromLabel);
        tvToLabel.setText(toLabel);
        tvEventType.setText(eventList.get(i).getEventType().getTypeName());
        tvEventType.setTextColor(Color.parseColor(eventList.get(i).getEventType().getColor()));
        if (startTimeFormated == null) {
            llTimeFrom.setVisibility(View.GONE);
        }
        else {
            tvEventStartTime.setText(startTimeFormated);
        }
        tvEventTitle.setText(eventList.get(i).getTitle());

        // both (toDate or toTime) and expense are present, show them all
        if ((toDateFormated != null || toTimeFormated != null) && (eventList.get(i).getExpenseAmount() != null)) {
            tvTimeFromTime.setText(startTimeFormated);
            tvTimeToDate.setText(toDateFormated);
            tvTimeToTime.setText(toTimeFormated);
            tvExpenseAmount.setText(eventList.get(i).getExpenseAmount());
            tvExpenseCurrency.setText(trip.getExpenseCurrency());
        }
        // either (toDate or toTime) or expense is present
        else {
            llDetails.setVisibility(View.GONE);
            llTitleExtra.setVisibility(View.VISIBLE);
            // show expense in extra view
            if (eventList.get(i).getExpenseAmount() != null) {
                tvTitleExtraKey.setText("Expense");
                tvTitleExtraValue.setText(eventList.get(i).getExpenseAmount());
                tvTitleExtraValue.setVisibility(View.VISIBLE);
                tvTitleExtraValueBtm.setText(trip.getExpenseCurrency());
                tvTitleExtraValueBtm.setAllCaps(true);
                tvTitleExtraValueBtm.setVisibility(View.VISIBLE);
            }
            // show date/time in extra view
            else if (toTimeFormated != null || toDateFormated != null){
                tvTitleExtraKey.setText(toLabel);
                if (toTimeFormated != null) {
                    tvTitleExtraValue.setText(toTimeFormated);
                    tvTitleExtraValue.setVisibility(View.VISIBLE);
                }
                if (toDateFormated != null) {
                    tvTitleExtraValueBtm.setText(toDateFormated);
                    tvTitleExtraValueBtm.setVisibility(View.VISIBLE);
                }
            }
        }
        ivEventIcon.setImageDrawable(eventList.get(i).getChildTypeIcon(context));
        ivEventIcon.setColorFilter((Color.parseColor(eventList.get(i).getEventType().getColor())));

        return view;
    }
}
