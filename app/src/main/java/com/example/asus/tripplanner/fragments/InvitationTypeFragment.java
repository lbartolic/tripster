package com.example.asus.tripplanner.fragments;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.example.asus.tripplanner.R;
import com.example.asus.tripplanner.activities.HomeActivity;
import com.example.asus.tripplanner.activities.InvitationsActivity;
import com.example.asus.tripplanner.activities.NewTripActivity;
import com.example.asus.tripplanner.activities.TripActivity;
import com.example.asus.tripplanner.activities.TripPlanActivity;
import com.example.asus.tripplanner.adapters.InvitationsListViewAdapter;
import com.example.asus.tripplanner.adapters.TripsListViewAdapter;
import com.example.asus.tripplanner.models.Invitation;
import com.example.asus.tripplanner.models.Trip;
import com.example.asus.tripplanner.models.User;
import com.example.asus.tripplanner.utils.ListUtil;
import com.example.asus.tripplanner.webservices.TripPlanner.interfaces.TripListResponse;
import com.example.asus.tripplanner.webservices.TripPlanner.repositories.TripRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 15.08.16..
 */
public class InvitationTypeFragment extends Fragment {
    private Type listType = new TypeToken<ArrayList<Invitation>>(){}.getType();
    private List<Invitation> invitationList;
    private InvitationsListViewAdapter lvAdapter;
    private ListView lvInvitations;
    private SwipeRefreshLayout srlInvitations;
    private String invitationsType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_invitations, container, false);
        lvInvitations = (ListView) v.findViewById(R.id.lvInvitations);

        invitationsType = getArguments().getString("TYPE");
        String invitationListJson = getArguments().getString("INVITATIONS");
        invitationList = new Gson().fromJson(invitationListJson, listType);

        srlInvitations = (SwipeRefreshLayout) v.findViewById(R.id.srlInvitations);

        setEventListeners();
        setInvitationList();
        return v;
    }

    private void setEventListeners() {
        srlInvitations.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ((InvitationsActivity) getActivity()).refreshData(InvitationTypeFragment.this, false);
            }
        });
    }

    private void setInvitationList() {
        lvAdapter = new InvitationsListViewAdapter(getActivity(), invitationList, invitationsType);
        lvInvitations.setAdapter(lvAdapter);
        lvInvitations.setFocusable(false);
    }

    public void stopSwipeRefresh() {
        srlInvitations.setRefreshing(false);
    }
}
