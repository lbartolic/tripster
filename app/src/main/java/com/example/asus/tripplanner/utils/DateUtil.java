package com.example.asus.tripplanner.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by asus on 17.08.16..
 */
public class DateUtil {
    public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
    public static final String DB_TIME_FORMAT = "HH:mm:ss";
    public static final String DB_TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String APP_DATE_FORMAT_DATE = "MMMM d, yyyy";
    public static final String APP_DATE_FORMAT_LONG = "EEE, MMM d, yyyy";
    public static final String APP_DATE_FORMAT_LONGER = "EEEE, MMMM d, yyyy";
    public static final String APP_DATE_FORMAT_SHORT = "EEE, MMM d";
    public static final String APP_TIME_FORMAT = "h:mm a";

    public static Date convertStringToDate(String dateString, String inputDateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(inputDateFormat);
        try {
            Date inputDate = sdf.parse(dateString);
            return inputDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertDateToString(Date date, String outputDateFormat) {
        SimpleDateFormat sdfOut = new SimpleDateFormat(outputDateFormat);
        return sdfOut.format(date);
    }

    public static String formatDateString(String inputDateString, String inputDateFormat, String outputDateFormat) {
        Date inputDate = DateUtil.convertStringToDate(inputDateString, inputDateFormat);
        String outputDate = DateUtil.convertDateToString(inputDate, outputDateFormat);
        return outputDate;
    }
}
