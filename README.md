# Tripster #
**Trip planner Android application**

Luka Bartolic

FERIT, Osijek, Croatia




##** TODO **##

* reuse code with similar functionality (reduce unnecessary amount of code)
* * some parts of code are messy and there is a possibility of code reducement (clean-up)
* handle errors, improve try-catch blocks, error messages in UI


**EXTRA TODO**
[Croatian]

* poboljšana autorizacija slanja zahtjeva prema API-ju
* privilegije – omogućiti različite privilegije (dopuštenja) korisnika u grupi putovanja gdje vlasnik (kreator) grupe ima najveća dopuštenja, a ostalim korisnicima može dodjeljivati željena prava za izmjenjivanje, stvaranje i brisanje događaja
* različita filtriranja i sortiranja planova putovanja, događaja unutar plana putovanja, korisnika i sl.
* osim rasporeda događaja putovanja prema danima, razviti i raspored trajanja događaja s prikazom u kalendaru gdje bi početak i kraj svakog događaja bili jasno naznačeni
* detaljni profili korisnika dostupni i ostalim korisnicima (dodavanje u favorite,...)
* prikaz i mogućnost vraćanja obrisanih dana i događaja putovanja
* detaljne obavijesti o aktivnosti unutar grupe putovanja – vrlo korisna nadogradnje gdje bi svi korisnici bili informirani o aktivnostima ostalih korisnika unutar zajedničke grupe koje uključuju obavijesti o izmjeni, dodavanju ili brisanju događaja
* chat u stvarnom vremenu za svaku grupu putovanja (omogućio bi korisnicima lakšu razmjenu ideja i lakše planiranje bez potrebe za dodatnom chat aplikacijom)
* **cachiranje** (spremanje u privremenu memoriju) i omogućavanje offline načina rada (dobra logika cachiranja bi omogućila brže učitavanje resursa, a s druge strane mogućnost pregledavanja planova putovanja bez internetske veze)
* upravljanje greškama i prikaz određenih poruka korisnicima kako bi bili bolje informirani (poput pogrešaka s internetskom vezom i sl.)
* mjenjačnica ("Box Office") – dodatak aplikaciji koji bi omogućio korisnicima brzu konverziju valuta kad su već na putovanju
* omogućiti generiranje prijedloga mjesta za posjetiti unutar kategorija aplikacije u kombinaciji s Google Places uslugom i korisnikovog odabira mjesta gdje putuje (prijedlozi za smještaj, restorani, i slično na temelju kvalitete, ocjena i udaljenosti od korisnikovog mjesta ili prijedlozi znamenitosti koje bi korisnici trebali posjetiti)
* vremenska prognoza za označene lokacije na mapi događaja (može biti dostupna prije samog putovanja ali bitno je da bude dostupna za vrijeme trajanja isplaniranog putovanja za označene događaje)